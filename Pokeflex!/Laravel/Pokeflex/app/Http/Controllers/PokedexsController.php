<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pokedex;

class PokedexsController extends Controller
{
    public function index() {
        return Pokedex::all();
    }

    public function byName($name) {
        $p = Pokedex::all()->where('name', '=', $name);
        
        return $p;
    }

    public function byId($id) {
        $p = Pokedex::all()->where('number', '=', $id);
        return $p;
    }

}
