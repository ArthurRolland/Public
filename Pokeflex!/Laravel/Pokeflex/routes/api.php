<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/pokedex', 'PokedexsController@index');
Route::get('/pokedex/{arg}', 'PokedexsController@byName')->where('arg', '[a-zA-Z]+');
Route::get('/pokedex/{arg}', 'PokedexsController@byId')->where('arg', '[0-9]+');
