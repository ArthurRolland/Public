let express = require('express');
let mysql = require('mysql');
let app = express();
let connection = mysql.createConnection({
    host: 'localhost', 
    port: 8889,
    user: 'root', 
    password: 'root', 
    database: 'pokeflex',
});
connection.connect();



app.get('/pokedex', (req, res) => {
    connection.query('SELECT * FROM Pokedex;', (error, results, fields) => {
        let r = results.map((o) => { return { id: o.number, name: o.name }; })
        res.json(r);
    });
});

app.get('/pokedex/:val', (req, res) => {
    let val = req.params['val'];
    let query = 'SELECT * FROM Pokedex WHERE name=?;'
    if (!isNaN(parseInt(val))) {
        query = 'SELECT * FROM Pokedex WHERE number=?;'
    }

    connection.query(query, val , (error, results, fields) => {
        res.json(results);
    });
    
});





app.listen(8885);