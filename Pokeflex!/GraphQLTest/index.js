var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');
let mysql = require('mysql');


class Database {
    constructor() {
        this.connection = mysql.createConnection({
            host: 'localhost', 
            port: 8889,
            user: 'root', 
            password: 'root', 
            database: 'pokeflex',
        });
    }
    query( sql, args ) {
        return new Promise( ( resolve, reject ) => {
            this.connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }
    close() {
        return new Promise( ( resolve, reject ) => {
            this.connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
}
let db = new Database();



class Pokedex {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

var schema = buildSchema(`
    type Pokedex {
        id: Int,
        name: String
    }

    type Query {
        pokedex(id: Int, name: String): [Pokedex]
    }
`);

var root = { 
    pokedex: ({id, name}) => {
        let sql = 'SELECT * FROM Pokedex';
        let args = [];
        if (id) {
            sql += " WHERE number = ?";
            args = [id];
        }
        else if (name) {
            sql += " WHERE name = ?";
            args = [name];
        }

        return db.query(sql, args)
        .then(rows => {
            let array = [];
            for (obj of rows) {
                let p = new Pokedex(obj.number, obj.name);
                array.push(p);
            }
            return array;
        });
    }
};

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));