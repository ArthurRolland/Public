<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/pokedex', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Pokeflex '/' pokedex");
    $pdo = $this->db;
    $request = $pdo->query('SELECT number, name FROM Pokedex;');
    $response = $request->fetchAll(PDO::FETCH_ASSOC);
    
    return json_encode($response);
});

$app->get('/pokedex/{value}', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Pokeflex '/' pokedex '/' id");
    $pdo = $this->db;

    $val = $args['value'];
    if (is_numeric($val)) {
        $sql = 'SELECT number, name FROM Pokedex WHERE number = :id;';
        $params = ['id' => $val];
    }
    else {
        $sql = 'SELECT number, name FROM Pokedex WHERE name = :name;';
        $params = ['name' => $val];
    }

    $request = $pdo->prepare($sql);
    $request->execute($params);
    $response = $request->fetchAll(PDO::FETCH_ASSOC);
    
    return json_encode($response);
});