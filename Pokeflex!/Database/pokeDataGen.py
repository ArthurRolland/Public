import urllib.request
import os, ssl
import json

types = ['bug', 'dragon','electric','fighting','fire','flying','ghost','grass','ground','ice','normal','poison','psychic','rock','water']

if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
    getattr(ssl, '_create_unverified_context', None)): 
    ssl._create_default_https_context = ssl._create_unverified_context


def getPokemon(id):
    req = urllib.request.Request('https://pokeapi.co/api/v2/pokemon/' + str(id) + '/', headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urllib.request.urlopen(req).read()
    s = webpage.decode('utf-8')
    jsonData = json.loads(s)

    name = jsonData["name"] 
    speed = int(jsonData["stats"][0]["base_stat"])
    spe = int((jsonData["stats"][1]["base_stat"] + jsonData["stats"][2]["base_stat"] ) / 2)
    defense = int(jsonData["stats"][3]["base_stat"])
    atk = int(jsonData["stats"][4]["base_stat"])
    hp = int(jsonData["stats"][5]["base_stat"])
    types = [ e['type']['name'] for e in jsonData['types']]
    
    s = "(" + str(id) + ", '" + name + "', " + str(hp) + ", " + str(atk) + ", " + str(defense) + ", " + str(speed) + ", " + str(spe) + ")"
    return (s, id, types)

def getAllPokemons():
    allPoke = []
    allTypes = []
    for i in range(1, 151):
        t = getPokemon(i)
        allPoke.append(t[0])
        for type in t[2]:
            try:
                i = types.index(type) + 1
                allTypes.append("(" + str(t[1]) + ", " + str(i) + ")")
            except:
                pass
    
    pokeStr = "INSERT INTO `Pokedex` (`number`, `name`, `hp`, `atk`, `def`, `speed`, `spe`) \nVALUES\n"
    pokeStr += ",\n".join(allPoke) + ";"

    typesStr = "INSERT INTO `PokedexHasType` \nVALUES\n"
    typesStr += ",\n".join(allTypes) + ";"
    return (pokeStr, typesStr)
        



def getEvolution(id):
    req = urllib.request.Request('https://pokeapi.co/api/v2/evolution-chain/' + str(id) + '/', headers={'User-Agent': 'Mozilla/5.0'})
    webpage = urllib.request.urlopen(req).read()
    s = webpage.decode('utf-8')
    jsonData = json.loads(s)
    res = flattenEvoChain(jsonData["chain"])
    if res == False:
        return False
    elif res == None:
        return None
    array = []
    for t in res:
        s = "UPDATE `Pokedex` SET `evolutionLvl` = " + str(t[1]) + ", `evolutionNumber` = " + str(t[2]) + " WHERE `number` = " + str(t[0]) + ";"
        array.append(s)
    return array

def flattenEvoChain(chain):
    if len(chain['evolves_to']) == 0: 
        return None
    url = chain['species']['url'].split("/")
    baseId = url[len(url) - 2]
    
    nextLink = chain['evolves_to'][0]
    evoUrl = nextLink['species']['url'].split("/")
    evoId = evoUrl[len(url) - 2]
    if int(baseId) > 150 and int(evoId) > 150:
        return False
    elif int(baseId) > 150:
        return None
    elif int(evoId) > 150:
        return None

    lvl = nextLink['evolution_details'][0]['min_level']

    res = flattenEvoChain(nextLink)
    lvl = -1 if lvl == None else lvl
    r = (int(baseId), lvl, int(evoId))
    if res == None:
        return [r]
    else:
        res.append(r)
        return res


def getAllEvolutions():
    all = []
    loop = True
    i = 1
    while loop:
        res = getEvolution(i)
        if res == False:
            loop = False
        elif res != None:
            for s in res:
                all.append(s)
        i += 1

    return "\n".join(all)
