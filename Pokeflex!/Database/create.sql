-- MySQL Script generated by MySQL Workbench
-- Wed Dec  5 16:26:06 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP DATABASE IF EXISTS `Pokeflex`;
CREATE DATABASE `Pokeflex`;
USE `Pokeflex`;

-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `pass` VARCHAR(256) NOT NULL,
  `token` VARCHAR(256) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idUser_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Pokedex`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pokedex` (
  `number` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `hp` INT NOT NULL,
  `atk` INT NOT NULL,
  `def` INT NOT NULL,
  `speed` INT NOT NULL,
  `spe` INT NOT NULL,
  `evolutionNumber` INT NULL,
  `evolutionLvl` INT NULL,
  UNIQUE INDEX `number_UNIQUE` (`number` ASC),
  INDEX `fk_Pokedex_Pokedex1_idx` (`evolutionNumber` ASC),
  CONSTRAINT `fk_Pokedex_Pokedex1`
    FOREIGN KEY (`evolutionNumber`)
    REFERENCES `Pokedex` (`number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Pokemon`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Pokemon` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hp` INT NOT NULL,
  `atk` INT NOT NULL,
  `def` INT NOT NULL,
  `speed` INT NOT NULL,
  `spe` INT NOT NULL,
  `name` VARCHAR(255) NULL,
  `lvl` INT NOT NULL,
  `numberPokedex` INT NOT NULL,
  `idUser` INT NOT NULL,
  `order` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Pokemon_Pokedex1_idx` (`numberPokedex` ASC),
  INDEX `fk_Pokemon_User1_idx` (`idUser` ASC),
  CONSTRAINT `fk_Pokemon_Pokedex1`
    FOREIGN KEY (`numberPokedex`)
    REFERENCES `Pokedex` (`number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pokemon_User1`
    FOREIGN KEY (`idUser`)
    REFERENCES `User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `PokedexHasType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PokedexHasType` (
  `pokedexNumber` INT NOT NULL,
  `typeId` INT NOT NULL,
  PRIMARY KEY (`pokedexNumber`, `typeId`),
  INDEX `fk_Pokedex_has_Type_Type1_idx` (`typeId` ASC),
  INDEX `fk_Pokedex_has_Type_Pokedex_idx` (`pokedexNumber` ASC),
  CONSTRAINT `fk_Pokedex_has_Type_Pokedex`
    FOREIGN KEY (`pokedexNumber`)
    REFERENCES `Pokedex` (`number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pokedex_has_Type_Type1`
    FOREIGN KEY (`typeId`)
    REFERENCES `Type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Advantage`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Advantage` (
  `mainTypeId` INT NOT NULL,
  `againstTypeId` INT NOT NULL,
  `modificator` FLOAT NOT NULL,
  PRIMARY KEY (`mainTypeId`, `againstTypeId`),
  INDEX `fk_Type_has_Type_Type2_idx` (`againstTypeId` ASC),
  INDEX `fk_Type_has_Type_Type1_idx` (`mainTypeId` ASC),
  CONSTRAINT `fk_Type_has_Type_Type1`
    FOREIGN KEY (`mainTypeId`)
    REFERENCES `Type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Type_has_Type_Type2`
    FOREIGN KEY (`againstTypeId`)
    REFERENCES `Type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
