<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pokemon
 *
 * @ORM\Table(name="Pokemon", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_Pokemon_Pokedex1_idx", columns={"numberPokedex"}), @ORM\Index(name="fk_Pokemon_User1_idx", columns={"idUser"})})
 * @ORM\Entity
 */
class Pokemon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="hp", type="integer", nullable=false)
     */
    private $hp;

    /**
     * @var int
     *
     * @ORM\Column(name="atk", type="integer", nullable=false)
     */
    private $atk;

    /**
     * @var int
     *
     * @ORM\Column(name="def", type="integer", nullable=false)
     */
    private $def;

    /**
     * @var int
     *
     * @ORM\Column(name="speed", type="integer", nullable=false)
     */
    private $speed;

    /**
     * @var int
     *
     * @ORM\Column(name="spe", type="integer", nullable=false)
     */
    private $spe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="lvl", type="integer", nullable=false)
     */
    private $lvl;

    /**
     * @var int
     *
     * @ORM\Column(name="order", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var \Pokedex
     *
     * @ORM\ManyToOne(targetEntity="Pokedex")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="numberPokedex", referencedColumnName="number")
     * })
     */
    private $numberpokedex;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     * })
     */
    private $iduser;


}
