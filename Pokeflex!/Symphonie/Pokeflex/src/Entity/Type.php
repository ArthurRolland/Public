<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table(name="Type", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})})
 * @ORM\Entity
 */
class Type
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Type", inversedBy="maintypeid")
     * @ORM\JoinTable(name="advantage",
     *   joinColumns={
     *     @ORM\JoinColumn(name="mainTypeId", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="againstTypeId", referencedColumnName="id")
     *   }
     * )
     */
    private $againsttypeid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Pokedex", mappedBy="typeid")
     */
    private $pokedexnumber;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->againsttypeid = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pokedexnumber = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
