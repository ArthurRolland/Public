<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pokedex
 *
 * @ORM\Table(name="Pokedex", uniqueConstraints={@ORM\UniqueConstraint(name="number_UNIQUE", columns={"number"})}, indexes={@ORM\Index(name="fk_Pokedex_Pokedex1_idx", columns={"evolutionNumber"})})
 * @ORM\Entity
 */
class Pokedex
{
    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="hp", type="integer", nullable=false)
     */
    private $hp;

    /**
     * @var int
     *
     * @ORM\Column(name="atk", type="integer", nullable=false)
     */
    private $atk;

    /**
     * @var int
     *
     * @ORM\Column(name="def", type="integer", nullable=false)
     */
    private $def;

    /**
     * @var int
     *
     * @ORM\Column(name="speed", type="integer", nullable=false)
     */
    private $speed;

    /**
     * @var int
     *
     * @ORM\Column(name="spe", type="integer", nullable=false)
     */
    private $spe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="evolutionLvl", type="integer", nullable=true)
     */
    private $evolutionlvl;

    /**
     * @var \Pokedex
     *
     * @ORM\ManyToOne(targetEntity="Pokedex")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="evolutionNumber", referencedColumnName="number")
     * })
     */
    private $evolutionnumber;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Type", inversedBy="pokedexnumber")
     * @ORM\JoinTable(name="pokedexhastype",
     *   joinColumns={
     *     @ORM\JoinColumn(name="pokedexNumber", referencedColumnName="number")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="typeId", referencedColumnName="id")
     *   }
     * )
     */
    private $typeid;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->typeid = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function resp() {
        return ["name" => $this->name];
    }

}
