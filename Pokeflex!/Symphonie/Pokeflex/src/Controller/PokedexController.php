<?php
    namespace App\Controller;

    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\JsonResponse;

    class PokedexController extends AbstractController {

        public function getAll() {
            $repository = $this->getDoctrine()->getRepository(\App\Entity\Pokedex::class);
            $query = $repository->createQueryBuilder('p')->getQuery();
            return new JsonResponse($query->getArrayResult());
        }

        public function byId($id) {
            $repository = $this->getDoctrine()->getRepository(\App\Entity\Pokedex::class);
            $pokedex = $repository->find($id);
            return new JsonResponse($pokedex->resp());
        }

        public function byName($name) {
            $repository = $this->getDoctrine()->getRepository(\App\Entity\Pokedex::class);
            $pokedex = $repository->findOneBy(["name" => $name]);
            return new JsonResponse($pokedex->resp());
        }

    }