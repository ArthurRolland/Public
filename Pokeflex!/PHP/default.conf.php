<?php

    // ! Error reporting
    // We activate different settings to be sure that the errors are shown
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    // ! FileSystem Const
    // We create constants referring to differents folders of our file structure
    define('INCLUDE_PATH', realpath(dirname(dirname(__FILE__))) . '/');


    // ! MySQL Const
    const DB_DRIVER = 'mysql';
    const DB_HOST = 'localhost';
    const DB_NAME = 'Pokeflex';
    const DB_USER = 'root';
    const DB_PASS = 'root';

    

?>