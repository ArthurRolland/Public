<?
    require_once('setup.php');
    $pdo = getPdo();
    if (isset($_GET['id'])) {
        $sql = "SELECT * FROM Pokedex WHERE number=:id";
        $request = $pdo->prepare($sql);
        $request->execute(["id" => $_GET['id']]);
        $res = $request->fetchAll(PDO::FETCH_ASSOC);
        sendJson($res);
    }
    else if (isset($_GET['name'])) {
        $sql = "SELECT * FROM Pokedex WHERE name=:name";
        $request = $pdo->prepare($sql);
        $request->execute(["name" => $_GET['name']]);
        $res = $request->fetchAll(PDO::FETCH_ASSOC);
        sendJson($res);
    }
    else {
        $sql = "SELECT number, name FROM Pokedex";
        $request = $pdo->query($sql);
        $res = $request->fetchAll(PDO::FETCH_ASSOC);
        sendJson($res);
    }

?>