<?php
    // ? This helper file will contain all usefull functions that we may need
    // ! Sending JSON
    // Main sending method
    /**
     * send an object as a json file, with the apropriate header
     * 
     * $object can be an array of DBObject or a single DBObject
     */
    function sendJson($data) {
        // Sending Header
        header('Content-Type: application/json');
        // Sending JSON
        echo json_encode($data, JSON_PRETTY_PRINT);
    }


    // ! PDO
    function getPdo() {
        $dsn = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
        try {
            $db = new PDO($dsn, DB_USER, DB_PASS, null);
            return $db;
        } catch (PDOException $e) {
            echo "Error connecting database: " . $e->getMessage();
        }
        return false;
        
    }

    
?>