<!DOCTYPE html>
<html lang="">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Test PHP</title>

</head>
<body>
<?php 
    // Exercice 2
    function syracuse($x) {
        do {
            if ($x > 0) {
                if ($x % 2 == 0) {
                    $x = $x / 2;
                }
                else {
                    $x = $x * 3 + 1;
                }
            }
            else {
                return NULL;
            }
            echo $x . " ";
        } while ($x > 1);
    }
    // syracuse(9);


    // Exercice 3
    // - 1
    function stars($len) {
        $str = "";
        while (strlen($str) < $len) {
            $str .= "*";
        }
        return $str;
    }
    // echo stars(8);

    // - 2
    function starSquare($size) {
        $str = "";
        for ($i = 0; $i < $size; $i++) {
            $str .= stars($size) . "\n";
        }
        return $str;
    }
    // echo "<pre>" . starSquare(4) . "</pre>";

    // - 3
    function symboleLine($s1, $s2, $l1, $l2, $hollowed = false) {
        $str = "";
        for ($i = 0; $i < $l1; $i++) {
            if ($hollowed && $i != 0 && $i != $l1 - 1) {
                $str .= " ";
            }
            else {
                $str .= $s1;
            }
        }
        for ($i = 0; $i < $l2; $i++) {
            if ($hollowed && $i != 0 && $i != $l2 - 1) {
                $str .= " ";
            }
            else {
                $str .= $s2;
            }
        }
        return $str;
    }

    function starTriangle($size, $type, $hollowed = false) {
        $str = "";
        for ($i = 0; $i < $size; $i++) {
            $hollowLine = ($hollowed && $i != 0 && $i != $size - 1);
            switch ($type){
                case 0:
                    $str .= symboleLine("*", " ", $i + 1, $size - $i + 1, $hollowLine);
                    break;
                case 1:
                    $str .= symboleLine(" ", "*", $i, $size - $i, $hollowLine);
                    break;
                case 2:
                    $str .= symboleLine(" ", "*", $size - $i, $i, $hollowLine);
                    break;
                case 3:
                    $str .= symboleLine("*", " ", $size - $i, $i, $hollowLine);
                    break;
                default: 
                    break;
            }
            $str .= "\n";
            
        }
        return $str;
    }
    // echo "<pre>" . starTriangle(16, 0) . "</pre>";
    // echo "<pre>" . starTriangle(16, 1) . "</pre>";
    // echo "<pre>" . starTriangle(16, 2) . "</pre>";
    // echo "<pre>" . starTriangle(16, 3) . "</pre>";


    // echo "<pre>" . starTriangle(160, 0, true) . "</pre>";
    // echo "<pre>" . starTriangle(16, 1, true) . "</pre>";
    // echo "<pre>" . starTriangle(16, 2, true) . "</pre>";
    // echo "<pre>" . starTriangle(16, 3, true) . "</pre>";

    // - 4
    $tab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    $sum = 0;
    $above = 0;
    $twenty = false;
    $best = 0;
    echo "Tab = ";
    for ($i = 0; $i < count($tab); $i++) { 
        echo $tab[$i] . " ";
        $sum += $tab[$i];
        if ($tab[$i] > 10) {
            $above++;
        }
        if ($tab[$i] == 20) {
            $twenty = true;
        }
        if($tab[$i] > $best) {
            $best = $tab[$i];
        }
    }
    echo "<br />";
    echo "Total = " . $sum;
    echo "<br />";
    echo "Avg = " . $sum / count($tab);
    echo "<br />";
    echo "Above 10 = " . $above;
    echo "<br />";
    if ($twenty) {
        echo "20? = YES";
    }
    else {
        echo "20? = NO";
    }

    echo "<br />";
    echo "Best = " . $best;
    

?>
</body>
</html>