<?php 
    require_once("text.php");

    function divide($a, $b) {
        superEcho("Trying to divide: " . $a . " by " . $b);
        if ($b == 0) {
            throw new Exception('Division par zero.');
        }
        else if ($b == 3) {
            throw new Exception('Division par trois.');
        }
        if ($b == 512) {
            throw new Exception('Division par 512.');
        }
        return $a / $b;
    } 
?>
