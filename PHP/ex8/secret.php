<?php
    session_start();
    if (!isset($_SESSION['login'])) {
        header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FBI</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <header>
        <h1>Bienvenu sur le site du FBI!</h1>
    </header>
    <div id="container">
        <section>
            <h1>Salut <?= $_SESSION['login'] ?></h1>
            <a href="logout.php">Se déconnecter</a> 
        </section>
    </div>
</body>

</html>