<?php
    session_start();
    require_once('helper.php');
    $isLoggedIn = false;
    $login = '';
    $database = getDatabase();
    
    if (isset($_POST['login']) && $_POST['pass']) {
        $login = $_POST['login'];
        $pass = $_POST['pass'];

        if (isset($database[$login])) {
            $savedHash = $database[$login];
            $testHash = customHash($login, $pass);
            $isLoggedIn = ($savedHash == $testHash);
        }
    }


    if ($isLoggedIn) {
        $_SESSION['login'] = $login;
        header('Location: secret.php');
    } else {
        header('Location: index.php');
    }
?>