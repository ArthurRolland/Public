<?php
    session_start();
    require_once('helper.php');
    if (isset($_SESSION['login'])) {
        header('Location: secret.php');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FBI</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <header>
        <h1>Bienvenu sur le site du FBI!</h1>
    </header>
    <div id="container">
        <section>
            <form id="loginForm" action="login.php" method="POST">
                <div>
                    <h1>Connectez vous!</h1>
                </div>
                <div>
                    <label>Nom d'utilisateur</label>
                    <input name="login" type="text" placeholder="Entrez votre nom" value="<?= isset($_GET["login"]) ? stripslashes($_GET["login"]) : '' ?>" />
                </div>
                <div>
                    <label>Mot de passe</label>
                    <input name="pass" type="password" placeholder="Mot de passe" />
                </div>
                <div>
                    <input id="submitButton" type="submit" value="Se connecter" />
                </div>
                <p>
                    Pas de compte? <a href="register.php">inscrivez vous!</a>
                </p>
            </form>
        </section>
    </div>
</body>

</html>