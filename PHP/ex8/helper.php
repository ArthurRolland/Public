<?php

    function customHash($a, $b) {
        $aa = sha1($a);
        $bb = sha1($b);

        $s = '';
        for ($i = 0; $i < 40; $i++) {
            $x = ($i % 2 == 0) ? $aa : $bb;
            $s .= substr($x, $i, 1);
        }
        return sha1($s);
    }

    //! CSV with cariage return

    // function getDatabase() {
    //     $array = [];
    //     $ressource = fopen('login', 'r');
    //     do {
    //         $string = trim(fgets($ressource));
    //         if ($string) {
    //             $parts = explode(',', $string);
    //             $key = $parts[0];
    //             $value = $parts[1];
    //             $array[$key] = $value;
    //         } 
    //     } while($string);
    //     fclose($ressource);
    //     return $array;
    // }

    // function register($login, $pass) {
    //     $hash = customHash($login, $pass);
    //     $ressource = fopen('login', 'a');
    //     $text = PHP_EOL . $login . ',' . $hash;
    //     fwrite($ressource, $text);
    //     fclose($ressource);
    // }


    //! Fixed size values
    function getDatabase() {
        $array = [];
        $ressource = fopen('login2', 'r');
        do {
            $key = trim(fread($ressource, 40));
            $value = fread($ressource, 40);
            if ($key && $value) {
                $array[$key] = $value;
            } 
        } while($key && $value);
        fclose($ressource);
        return $array;
    }

    

    function register($login, $pass) {
        $hash = customHash($login, $pass);
        $ressource = fopen('login2', 'a');
        $text = str_pad($login, 40, ' ') . $hash;
        fwrite($ressource, $text);
        fclose($ressource);
    }

    function customLog($message) {
        $ressource = fopen('log', 'a');
        $text = '[' . date('d/m/Y - h:i:s') . '] ' . $message . PHP_EOL;
        fwrite($ressource, $text);
    } 
?>