<?php
    $students = [];
    // Sert a mettre un titre au tableau
    $tableHead = '<th>Note 1</th>';
    if (isset($_POST['students'])) {
        $students = $_POST['students'];

        for ($j = 2; $j < count($students[0]); $j++) {
            // On ajoute le titre si il y a pleins de notes 
            $tableHead .= '<th>Note ' . $j . '</th>';
        }

        // On calcule les resultats
        $total = 0;
        $best = -1;
        $bestName = [];
        $above = 0;
        for ($i = 0; $i < count($students); $i++) {
            $name = $students[$i][0];
            $note = 0;
            // On calcule la moyenne d'un eleve
            for ($j = 1; $j < count($students[$i]); $j++) {
                $note += $students[$i][$j];
            }
            $note /= count($students[$i]) - 1;
            // et ensuite on calule les autres truc
            $total += $note;
            if ($note > 10) { $above++; }
            if ($note >= $best) {
                if ($note == $best) { $bestName[] = $name; }
                else {
                    $best = $note;
                    $bestName = [$name];
                }
            }
        }
        $moy = $total / count($students);
    }

    $str = '';
    for ($i = 0; $i < count($students); $i++) {
        // Afficher les notes
        $name = $students[$i][0];

        $str .= '<tr>';
        $str .=  '<td><input type="text" name="students['. $i .'][]" placeholder="Nom ?" value ="' . $name . '" readonly/></td>';
        // Si on a plusieurs notes, on ajoute plusieurs colonnes
        for ($j = 1; $j < count($students[$i]); $j++) {
            $note = $students[$i][$j];
            $str .=  '<td><input type="number" name="students['. $i .'][]" value=' . $note . ' readonly/></td>';
        }
        $str .=  '</tr>';
    }
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>My first form</title>
        <!-- Adding JavaScript file -->
        <script src="script.js"></script>
        <!-- Adding CSS file -->
        <link href="style.css" rel="stylesheet">
        
    </head>
    <body>
        <section id="content">
            <h1>Notes</h1>
            <button type="button" onClick="addRow();"> + </button><br/>
            <form action="index.php" method="POST">
                <table id="gradesTable">
                    <tr>
                        <th>Eleve</th>
                        <?= $tableHead ?>
                        <th onClick="event.preventDefault(); addCol();"><button >+</button></th>
                    </tr>
                    <?= $str ?>
                </table>
                
                <input type="submit" value="Envoyer" />
            </form>
        </section>
        <?php if (count($students) > 0): ?>
            <section>
                <h1>Resultats</h1>
                <ul>
                    <li>Moyenne générale: <?= $moy ?></li>
                    <li>Plus de 10 de moyenne: <?= $above ?></li>
                    <li>Meilleure moyenne: <?= $best . ' (' . implode(', ', $bestName) . ')'  ?></li>
                </ul>
            </section>
        <?php endif; ?>
    </body>

</html>
 