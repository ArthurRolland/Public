<?php
    $students = $_POST['students'];
    $total = 0;
    $above = 0;
    $best = -1;
    $bestStudent = [];
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>My first form - Results</title>
        <!-- Adding JavaScript file -->
        <script src="script.js"></script>
	    <!-- Adding CSS file -->
        <link href="style.css" rel="stylesheet">
        
    </head>
    <body>
        <section id="content">
            <h1>Resultats</h1>
            <table id="gradesTable">
                <tr>
                    <th>Eleve</th>
                    <th>Note</th>
                </tr>
                <?php
                    for ($i = 0; $i < count($students); $i++) {
                        // Afficher les notes
                        $name = $students[$i][0];
                        $note = $students[$i][1];

                        echo '<tr>';
                        echo '<td>' . $name . '</td>';
                        echo '<td>' . $note . '</td>';
                        echo '</tr>';

                        // Calculer de trucs
                        $total += $note;
                        if ($note > 10) {
                            $above++;
                        }

                        if ($note > $best) {
                            $best = $note;
                            $bestStudent = [$name];
                        }
                        else if ($note == $best) {
                            $bestStudent[] = $name;
                        }
                        
                    }
                    $moy = $total / count($students);
                ?>
            </table> 
            <ul>
                <li>Moyenne: <?= $moy ?></li> 
                <li>Plus de 10: <?= $above ?></li>
                <li>Meilleure note:  <?= implode(", ", $bestStudent) . ' (' . $best .')'?></li>
            </ul>
        </section>
    </body>

    <script>
        
    </script>
</html>
