function addRow() {
    const table = document.getElementById("gradesTable");
    const index = table.rows.length - 1;
    const row = table.insertRow();
    const notesCount = table.rows[0].cells.length - 2;
    
    const cell1 = row.insertCell(0);
    cell1.innerHTML = '<input type="text" name="students[' + index + '][]" placeholder="Nom ?" />';
    for (let i = 0; i < notesCount; i++ ) {
        const cell2 = row.insertCell();
        cell2.innerHTML = '<input type="number" name="students[' + index + '][]" value=10 />';
    }
}
function addCol() {
    const table = document.getElementById("gradesTable");
    const notesCount = table.rows[0].cells.length - 1;
    for (let i = 0; i < table.rows.length; i++) {
        const row = table.rows[i];
        if (i == 0) {
            const cell = row.insertCell(notesCount)
            cell.outerHTML = '<th>Note ' + (notesCount) + '</th>';
        }
        else {
            const cell = row.insertCell()
            cell.innerHTML = '<input type="number" name="students[' + (i - 1) + '][]" value=10 />';
        }
        
    }

}