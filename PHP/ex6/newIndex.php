<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>My first form</title>
        <!-- Adding JavaScript file -->
        <script src="script.js"></script>
        <!-- Adding CSS file -->
        <link href="style.css" rel="stylesheet">
        
    </head>
    <body>
        <section id="content">
            <h1>Notes</h1>
            <button type="button" onClick="addRow();"> + </button><br/>
            <form action="index.php" method="POST">
                <table id="gradesTable">
                    <tr>
                        <th>Eleve</th>
                        <th>Note 1</th>
                        <th onClick="event.preventDefault(); addCol();"><button >+</button></th>
                    </tr>
                </table>
                
                <input type="submit" value="Envoyer" />
            </form>
        </section>
    </body>

</html>