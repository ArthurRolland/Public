<?php
    session_start();
    require_once("helper.php");
    $errorMsg = "";

    if (isset($_SESSION['login'])) {
        header('Location: secret.php');
    }

    if (isset($_POST['login']) && isset($_POST['pass'])) {
        $login = $_POST['login'];
        $pass = $_POST['pass'];

        
        if (strlen($login) > 40) {
            $errorMsg = "Ce login est trop long";
        }
        else {
            if ($login != '' && $pass != '') {
                if (register($login, $pass)) {
                    header('Location: index.php?login=' . $login);
                }
                else {
                    $errorMsg = "Une erreur est survenue";
                }
            }
        }
    }
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FBI - Inscription</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <header>
        <h1>Rejoignez le FBI!</h1>
    </header>
    <div id="container">
        <section>
            <form id="loginForm" action="register.php" method="POST">
                <div>
                    <h1>Inscrivez vous!</h1>
                </div>
                <h2 style="color:red"><?= $errorMsg ?></h2>
                <div>
                    <label>Nom d'utilisateur</label>
                    <input name="login" type="text" placeholder="Entrez votre nom" />
                </div>
                <div>
                    <label>Mot de passe</label>
                    <input name="pass" type="password" placeholder="Mot de passe" />
                </div>
                <div>
                    <input id="submitButton" type="submit" value="S'inscrire" />
                </div>
            </form>
        </section>
    </div>
</body>

</html>