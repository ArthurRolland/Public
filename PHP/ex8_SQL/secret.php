<?php
    session_start();
    require_once('helper.php');
    if (!isset($_SESSION['id'])) {
        header('Location: index.php');
    }
    $user = getUser($_SESSION['id']);

    if (isset($_POST['message'])) {
        if ($_POST['message'] != '') {
            postMessage($user, $_POST['message']);
        }
    }

    $comments = getAllMessages();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FBI</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <header>
        <h1>Bienvenu sur le site du FBI!</h1>
    </header>
    <div id="container">
        <section>
            <article>
                <h1>Salut <?= $user['name'] ?></h1>
                <a href="logout.php">Se déconnecter</a> 
            </article>
            <article>
                <h3>Leave a comment</h3>
                <form action="" method="POST">
                    <input name="message" placeholder="Type your message here" />
                    <input type="submit" value="send" />
                </form>
            </article>
            <article>
                <?php 
                    foreach ($comments as $comment) {
                        ?>
                <div>
                    <p>
                        <?= $comment['message'] ?>
                        <br/>
                        <em>- par <?= $comment['name'] ?></em>
                    </p>
                </div>
                        <?php
                    }
                
                ?>
            </article>
        </section>
    </div>
</body>

</html>