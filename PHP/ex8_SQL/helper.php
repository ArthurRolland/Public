<?php

    function customHash($a, $b) {
        $aa = sha1($a);
        $bb = sha1($b);

        $s = '';
        for ($i = 0; $i < 40; $i++) {
            $x = ($i % 2 == 0) ? $aa : $bb;
            $s .= substr($x, $i, 1);
        }
        return sha1($s);
    }

    


    function getPDO() {
        $dbDriver = 'mysql';
        $host = 'localhost';
        $dbname = 'Test2';

        $dsn =  $dbDriver . ':host='. $host .';dbname='. $dbname; // 'mysql:host=localhost;dbname=Test2'
        // $dsn est le Data Source Name, et contient les informations requises pour se connecter à la base.
        // Il est constitué du nom du pilote PDO, suivi d'une syntaxe spécifique au pilote.
        $user = 'root';
        $pass = 'root';
        $db = null;
        try {
            $db = new PDO($dsn, $user, $pass);
        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
        return $db;
    }

    function login($name, $pass) {
        $db = getPDO();
        $sql = 'SELECT * FROM User WHERE name = :userName';
        $request = $db->prepare($sql);
        $request->execute([':userName' => $name]);
        $res = $request->fetchAll(PDO::FETCH_ASSOC);
        if (count($res) > 0) {
            $user = $res[0];
            $hash = customHash($name, $pass);
            if ($user['pass'] == $hash) {
                return $user;
            }
        }
        return false;
    }

    function getUser($id) {
        $db = getPDO();
        $sql = 'SELECT * FROM User WHERE id = :id';
        $request = $db->prepare($sql);
        $request->execute([':id' => $id]);
        $res = $request->fetchAll(PDO::FETCH_ASSOC);
        if (count($res) > 0) {
            return $res[0];
        }
        return false;
    }

    function register($name, $pass) {
        $db = getPDO();
        $sql = 'SELECT * FROM User WHERE name = :userName';
        $request = $db->prepare($sql);
        $request->execute([':userName' => $name]);
        $res = $request->fetchAll(PDO::FETCH_ASSOC);
        if (count($res) == 0) {
            $hash = customHash($name, $pass);
            $sql = 'INSERT INTO User (name, pass) VALUES (:userName, :userPass);';
            $request = $db->prepare($sql);
            $request->execute([':userName' => $name, ':userPass' => $hash]);
            return true;
        }
        return false;
    }

    function getAllMessages() {
        $db = getPDO();
        $sql = 'SELECT c.message as `message`, c.postDate as `date`, u.name as `name` FROM Comment c JOIN  User u ON c.author = u.id ORDER BY c.postDate DESC;';
        $request = $db->query($sql);
        return $request->fetchAll(PDO::FETCH_ASSOC);
        
    }

    function postMessage($user, $message) {
        $db = getPDO();
        $sql = 'INSERT INTO Comment (message, author) VALUES (:message, :authorId);';
        $request = $db->prepare($sql);
        $res = $request->execute([':message' => $message, ':authorId' => $user['id']]);
        
    }
?>