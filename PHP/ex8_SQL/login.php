<?php
    session_start();
    require_once('helper.php');
    $user = false;
    $login = '';
    
    if (isset($_POST['login']) && $_POST['pass']) {
        $login = $_POST['login'];
        $pass = $_POST['pass'];

        $user = login($login, $pass);
    }


    if ($user == false) {
        header('Location: index.php');
    } else {
        $_SESSION['id'] = $user['id'];
        header('Location: secret.php');
    }
?>