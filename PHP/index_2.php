<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Sample php page</title>
        <!-- Adding JavaScript file -->
        <script src="script.js"></script>
	    <!-- Adding CSS file -->
        <link href="style.css" rel="stylesheet">
        
    </head>
    <body>
        <?php
            $grid = makeRandomGrid(200);
            createGrid($grid);

            function makeRandomGrid($size) {
                $grid = [];
                for ($i=0; $i < $size; $i++) { 
                    $grid[$i] = [];
                    for ($j=0; $j < $size; $j++) { 
                        $grid[$i][$j] = [rand(0, 255), rand(0, 255), rand(0, 255)];
                    }
                }
                return $grid;
            }

            function createGrid($grid) {
                echo '<div class="board">';
                for ($i=0; $i < count($grid); $i++) { 
                    createLine($grid[$i]);
                }
                echo '</div>';
            }
            function createLine($line) {
                echo '<div class="line">';
                for ($i = 0; $i < count($line); $i++) {
                    $tab = $line[$i];
                    $r = $tab[0];
                    $g = $tab[1];
                    $b = $tab[2];
                    echo '<div class="square" style="background-color: rgb('. $r .', '. $g .', '. $b .');"></div>';
                }
                echo '</div>'; 
            }
        ?>
    </body>

    <script>
        // Embeded JavaScript goes here
    </script>
</html>