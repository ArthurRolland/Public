<?php
    $age = 18;

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Ma super page!</title>
        <!--	Bulma lib	-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    </head>
    <body>
        <nav class="navbar is-primary" role="navigation"  aria-label="dropdown navigation">
            <div class="navbar-brand">
                <a class="navbar-item is-size-3" href="index.html">
                    <i class="fas fa-cash-register is-size-1"></i>
                    <h1>&emsp;Site pour adulte</h1>
                </a>
            </div>
            
            <div id="navbarBasicExample" class="navbar-menu">
            </div>
        </nav>
        <section class="section">
            <div class="tile is-ancestor">
                <?php if ($age < 18): ?> 
                    <div class="tile is-parent">
                        <article class="tile is-child notification is-danger">
                            <div class="content">
                                <p class="title">Entrée Interdite</p>
                                <p class="subtitle">Tu n'as pas l'age "requis" pour entrer, blabla...</p>
                                <div class="content">
                                    <!-- Content -->
                                </div>
                            </div>
                        </article>
                    </div>
                <?php else: ?>
                    <div class="tile is-vertical is-8">
                        <div class="tile">
                            <div class="tile is-parent is-vertical">
                                <article class="tile is-child notification is-primary">
                                    <p class="title">Bienvenu su mon super site</p>
                                    <p class="subtitle">Vien chercher du contenu pour adulte!</p>

                                    <select>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </article>
                                <article class="tile is-child notification is-warning">
                                    <p class="title">...Fire...</p>
                                    <p class="subtitle">Bottom tile</p>
                                </article>
                            </div>
                            <div class="tile is-parent">
                                <article class="tile is-child notification is-info">
                                    <p class="title">...Base</p>
                                    <p class="subtitle">With an image</p>
                                    <figure class="image is-4by3">
                                        <img src="https://bulma.io/images/placeholders/640x480.png">
                                    </figure>
                                </article>
                            </div>
                        </div>
                        <div class="tile is-parent">
                            <article class="tile is-child notification is-danger">
                                <p class="title">UTILISER FIREBASE</p>
                                <p class="subtitle">UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE UTILISER FIREBASE </p>
                                <div class="content">
                                    <!-- Content -->
                                </div>
                            </article>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </body>
</html>