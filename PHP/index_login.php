<?php
    include_once('math.php');
    include_once('text.php');

    
    $displayForm = true;
    if (isset($_GET['action']) && ($_GET['action'] == "connect" || $_GET['action'] == "create")) {
        $user = NULL;
        $pass = NULL;
        if (isset($_POST["user"])) { $user = $_POST["user"]; }
        if (isset($_POST["pass"])) { $pass = $_POST["pass"]; }
        if ($_GET['action'] == "create") {
            if (!isset($_POST["verifPass"]) || $_POST["verifPass"] != $pass) {
                $pass = NULL;
            }
        }


        if ($user && $pass) {
            $displayForm = false;
            $message = "";
            if ($_GET['action'] == "connect" ) {
                $message = "Welcome " . $user;
            }
            else {
                $message = "Creating account for " . $user;
            }
        }
        
    }
        


?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Sample php page</title>
        <!-- Adding JavaScript file -->
        <script src="script.js"></script>
	    <!-- Adding CSS file -->
        <!-- <link href="style.css" rel="stylesheet"> -->
        
    </head>
    <body>
        <?php if ($displayForm) { ?>
            <form action="?action=connect" method="POST">
                <h1>Connect</h1>
                <input type="text" placeholder="User name" name="user" />
                <input type="password" placeholder="Password" name="pass" />
                <input type="submit" value="send" />
            </form>
            <p>Or</p>
            <form action="?action=create" method="POST">
                <h1>Sign in</h1>
                <input type="text" name="user" />
                <input type="password" placeholder="Password" name="pass" />
                <input type="password" placeholder="Verify password" name="verifPass" />
                <input type="submit" value="send" />
            </form>
        <?php 
            } 
            else { 
                echo "<h1>" . $message . "</h1>";
            } 
        ?>

    </body>

    <script>
        // Embeded JavaScript goes here
    </script>
</html>