<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Sample php page</title>
        <!-- Adding JavaScript file -->
        <script src="script.js"></script>
	    <!-- Adding CSS file -->
        <link href="style.css" rel="stylesheet">
        
    </head>
    <body>
        <?php
            // 1
            createGrid(10);
            function createGrid($size) {
                echo '<div class="board">';
                for ($i=0; $i < $size; $i++) { 
                    createLine($size);
                }
                echo '</div>';
            }
            function createLine($size) {
                echo '<div class="line">';
                for ($i=0; $i < $size; $i++) {
                    echo '<div class="square"></div>';
                }
                echo '</div>'; 
            }
        ?>
    </body>

    <script>
        // Embeded JavaScript goes here
    </script>
</html>