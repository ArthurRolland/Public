import random

serverCount = 10
computerCount = 200

def rndMac():
    alphabet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    mac = "'"
    for i in range(0, 12):
        r = random.randint(0, len(alphabet) - 1)
        mac += alphabet[r]
    mac += "'"
    return mac

def rndIp(min, max, excluded):
    ip = ""
    while ip == "":
        r = random.randint(min, max)
        ip = "'192168001"+ str(r)+"'"
        if ip in excluded:
            ip = ""
    return ip

def createsServerComputers(osArray, serversModels, models, count, lastId):
    array = []
    for i in range(0, count):
        r = random.randint(0, 3)
        id = serversModels[r]
        model = models[id]
        id = model[0]
        eth = rndMac() if model[6] else "NULL"
        wifi = rndMac() if model[7] else "NULL"
        os = osArray[0] if id <= 2 else osArray[random.randint(1,len(osArray) - 1)]
        array.append((lastId+i+1, id, eth, wifi, os, 1))
    return array

def createsOtherComputers(osArray, serversModels, models, count, lastId):
    array = []
    for i in range(0, count):
        r = -1
        while(r == -1):
            r = random.randint(0, len(models) -1)
            if r in serversModelId: 
                r = -1
        model = models[r]
        id = model[0]
        eth = rndMac() if model[6] else "NULL"
        wifi = rndMac() if model[7] else "NULL"
        os = osArray[0] if id <= 2 else osArray[random.randint(1,len(osArray) - 1)]
        array.append((lastId+i+1, id, eth, wifi, os, 1))
    return array


def createServers(serverComputers, campuses):
    servers = []
    ips = {}
    i = 1
    for s in serverComputers:
        c = campuses[random.randint(0, len(campuses) - 1)]
        ex = []
        if c[0] in ips.keys():
            ex = ips[c[0]]
        ip = rndIp(200, 220, ex)
        ex.append(ip)
        ips[c[0]] = ex
        servers.append((i, s[0], c[0], ip))
        i += 1
    return servers

def createPort(servers, services):
    ports = []
    for serv in servers:
        x = random.randint(2, len(services) - 1)
        done = []
        while len(done) < x:
            i = random.randint(0, len(services) - 1)
            if i not in done:
                done.append(i)
                s = services[i]
                ports.append((serv[0], s[0], s[2]))
    return ports

def assignPrograms(campuses, programs):
    array = []
    for c in campuses:
        for p in programs:
            r = random.randint(0, 100)
            if r % 3 != 0:
                array.append((c[0], p[0], p[2]))

    return array

def createPromos(programsInCampuses, computers):
    total = 0
    promos = []
    i = 1
    while total < len(computers):
        studCount = random.randint(5, 25)
        p = programsInCampuses[random.randint(0, len(programsInCampuses) - 1)]
        y = random.randint(0, p[2] - 1)
        promos.append((i, 2018 - y, p[0], p[1], studCount))
        i += 1
        total += studCount 
    return promos

def createStudents(promo, names):
    n = list(names)
    stds = []
    i = 1
    for p in promos:
        for x in range(0, p[4]):
            if len(n) == 0:
                n = list(names)
            r = random.randint(0, len(n) - 1)
            name = n[r]
            n.remove(name)
            stds.append((i, name[0], name[1], p[0], p[1]))
            i += 1
    return stds

def createLoans(students, computers):
    loans = []
    sArray = list(students)
    cArray = list(computers)
    while len(sArray) > 0 and len(cArray) > 0:
        s = sArray[random.randint(0, len(sArray) - 1)]
        sArray.remove(s)
        c = cArray[random.randint(0, len(cArray) - 1)]
        cArray.remove(c)
        y = random.randint(s[4], 2018)
        loans.append((c[0], s[0],  str(y) + "-09-01"))
    return loans

services = [
    (1, 'SSH', 22), 
    (2, 'FTP', 21), 
    (3, 'SMTP', 25), 
    (4, 'Apache', 80), 
    (5, 'Tomcat', 8080), 
    (6, 'IRC', 6697), 
    (7, 'MySQL', 3306), 
    (8, 'PostgreSQL', 5432), 
    (9, 'OracleSQL', 1521)]
models = [
    (1, 'Apple', 'Mac Book Pro 15', 'Intel i7', 16, 500, False, True, 2013),
    (2, 'Apple', 'Mac Mini', 'Intel i7', 32, 1000, True, True, 2013),
    (3, 'HP', 'OMEN', 'Intel i5', 16, 1000, True, True, 2015),
    (4, 'HP', 'Envy', 'Intel i7', 8, 1000, False, True, 2017),
    (5, 'HP', 'OMEN Desktop', 'Intel i7', 32, 2000, True, False, 2018),
    (6, 'Acer', 'Swift 7 Pro', 'Intel i7', 8, 256, False, True, 2017),
    (7, 'Acer', 'Aspire 5 Pro 517-51GP ', 'Intel i5', 8, 1128, True, True, 2016),
    (8, 'Dell', 'Alienware 17', 'Intel i9', 16, 1128, True, True, 2018),
    (9, 'Dell', 'PowerEdge T430', 'Intel i7', 16, 1000, True, False, 2017),
    (10, 'Dell', 'PowerEdge T640', 'Intel i7', 8, 600, True, False, 2017)
]
serversModelId = [1 , 4, 8, 9]
os = ['Mac OS Mojave', 'Debian 9.8', 'Windows 10']

campuses = [
    (1, 'Caen', 14000),
    (2, 'Rennes', 35000),
    (3, 'Nantes', 44000),
    (4, 'Angers', 49000)
]

programs = [
    (1, "DEV", 5),
    (2, "ASR", 1),
    (3, "E-Sport", 1)
]

with open("randomNames.txt", 'r') as namesFile:
    s = namesFile.read()
    l = s.splitlines()
    randomNames = [n.split(" ") for n in l]


serverComputers = createsServerComputers(os, serversModelId, models, serverCount, 0)
# print(serverComputers)
otherComputers = createsOtherComputers(os, serversModelId, models, computerCount, len(serverComputers))
# print(otherComputers)
servers = createServers(serverComputers,campuses)
# print(servers)
ports = createPort(servers, services)
# print(ports)

programsInCampuses = assignPrograms(campuses, programs)
# print(programsInCampuses)

promos = createPromos(programsInCampuses, otherComputers)
# print(promos)

students = createStudents(promos, randomNames)
# print(students)

loans = createLoans(students, otherComputers)
# print(loans)

#  -- Services
print("INSERT INTO `Service`(`id`, `name`) VALUES")
i = 1
for s in services:
    xx = "(" + str(s[0]) + ", '" + s[1] + "')"
    if i != len(services):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Model
print("INSERT INTO `Model`(`id`, `constructor`, `number`, `pocesor`, `ram`, `hdd`, `eth`, `wifi`, `year`) VALUES")
i = 1
for m in models:
    eth = "1" if m[6] else "0"
    wifi = "1" if m[7] else "0"
    xx = "(" + str(m[0]) + ", '" + m[1] + "', '" + m[2] + "', '" + m[3] + "', " + str(m[4]) + ", " + str(m[5]) + ", " + str(eth) + ", " + str(wifi) + ", " + str(m[8]) + ")"
    if i != len(models):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Campus
print("INSERT INTO `Campus`(`id`, `name`, `zip`) VALUES")
i = 1
for s in campuses:
    xx = "(" + str(s[0]) + ", '" + s[1] + "', " + str(s[2]) + ")"
    if i != len(campuses):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Program
print("INSERT INTO `Program`(`id`, `name`, `maxLevel`) VALUES")
i = 1
for s in programs:
    xx = "(" + str(s[0]) + ", '" + s[1] + "', " + str(s[2]) + ")"
    if i != len(programs):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Computers
print("INSERT INTO `Computer`(`id`, `idModel`, `macEth`, `macWifi`, `os`, `status`) VALUES")
for s in serverComputers:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ", " + s[2] + ", " + s[3] + ", '" + s[4] + "', " + str(s[5]) + "),"
    print(xx)

i = 1
for s in otherComputers:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ", " + s[2] + ", " + s[3] + ", '" + s[4] + "', " + str(s[5]) + ")"
    if i != len(otherComputers):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Server
print("INSERT INTO `Server`(`id`, `idComputer`, `idCampus`, `ip`) VALUES")
i = 1
for s in servers:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ", " + str(s[2]) + ", " + s[3] + ")"
    if i != len(servers):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Port
print("INSERT INTO `Port`(`idServer`, `idService`, `number`) VALUES")
i = 1
for s in ports:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ", '" + str(s[2]) + "')"
    if i != len(ports):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- ProgramInCampus
print("INSERT INTO `ProgramInCampus`(`idCampus`, `idProgram`) VALUES")
i = 1
for s in programsInCampuses:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ")"
    if i != len(programsInCampuses):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Promo
print("INSERT INTO `Promo`(`id`, `year`, `idCampus`, `idProgram`) VALUES")
i = 1
for s in promos:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ", " + str(s[2]) + ", " + str(s[3]) + ")"
    if i != len(promos):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Student
print("INSERT INTO `Student`(`id`, `firstName`, `lastName`, `idPromo`) VALUES")
i = 1
for s in students:
    xx = "(" + str(s[0]) + ", '" + s[1] + "', '" + s[2] + "', " + str(s[3]) + ")"
    if i != len(students):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1

# -- Loan
print("INSERT INTO `Loan`(`idComputer`, `idStudent`, `start`, `end`) VALUES")
i = 1
for s in loans:
    xx = "(" + str(s[0]) + ", " + str(s[1]) + ", '" + s[2] + "', NULL)"
    if i != len(loans):
        xx += ","
    else:
        xx += ";"
    print(xx)
    i += 1
