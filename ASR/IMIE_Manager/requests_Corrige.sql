-- Les requetes demandées portens sur la Base de donnée IMIE_ASR, modélisée pendant les précédents cours.
-- Vous trouverez dans le dosier "BDD", deux fichier SQL permettant l'instalation de cette dernière.
-- Dans le dossier "Merise", vous trouverez la sauvegarde MySQL Workbench ayant servie à la modélisation de cette base, ainsi qu'un schema au format PNG


-- Durant cette FOAD, vous deverez implementer les requetes demandées et les sauvegarder dans ce document.


-- ! Requete 1:
-- Afficher le nom, le prénom et la promo et l'année d'entré de tout les éleves, trié par année

-- ? 
SELECT s.firstName, s.lastName, p.id, p.year
FROM Student s
JOIN Promo p ON p.id = s.idPromo
ORDER BY p.year;


-- ! Requete 2:
-- Afficher le nom, le prénom, la promo, l'année, le nom du programme, le nom du campus et le code postale du campus de chaques éleves
-- trié par campus (alphabetique), puis program (alphabetique), puis année

-- ?
SELECT s.firstName, s.lastName, p.id, p.year, pr.name, c.name, c.zip
FROM Student s
JOIN Promo p ON p.id = s.idPromo
JOIN Program pr ON pr.id = p.idProgram
JOIN Campus c ON c.id = p.idCampus
ORDER BY c.name, pr.name, p.year;

-- ! Requete 3:
-- Afficher le nom, le prénom et le nom de promo complet* de tous les etudiants de caen, trié par anciennetée
-- *Le nom complet d'une promo est composé comme suit:
--      "campusName"-"programName"-"ancienetée"
--      Par exemple, pour Liam kelly, dans la promo DEV 2016 de caen , le resultat sera:
--      "Caen-DEV-3"
-- (Attention avec MySQL, la concatenation de chaines se fait via la fonction CONCAT() - cf: https://www.w3schools.com/sql/func_mysql_concat.asp)

-- ?
SELECT s.firstName, s.lastName, p.year, CONCAT(c.name, "-", pr.name, "-", YEAR(SYSDATE()) - p.year)
FROM Student s
JOIN Promo p ON p.id = s.idPromo
JOIN Program pr ON pr.id = p.idProgram
JOIN Campus c ON c.id = p.idCampus
WHERE c.name = 'Caen';

-- ! Requete 4:
-- Afficher le nom, le prénom et les adresses MAC (eth et wifi) de chaque etudiants

-- ?
SELECT s.firstName, s.lastName, IFNULL(c.macEth, "N/A"), IFNULL(c.macWifi, "N/A")
FROM Student s
LEFT JOIN Loan l ON s.id = l.idStudent
LEFT JOIN  Computer c ON c.id = l.idComputer
LIMIT 0, 1000

-- ! Requete 5:
-- Afficher le nom, l'ip et le port des chaque services disponnible pour un etudiant, en fonction de son nom et prénom, trié par ip et par port
-- (Par exemple, pour Jackson Wilson, ne renvoyer que les services disponnible dans son campus, soit:
--      Apache - 192168001203 -80
--      IRC - 192168001203 - 6697
--      PostgreSQL - 192168001203 - 5432
--      MySQL - 192168001203 - 3306
--      SSH - 192168001203 - 22
--      FTP - 192168001203 - 21
--      OracleSQL - 192168001203 - 1521
--      Tomcat - 192168001207 - 8080
--      PostgreSQL - 192168001207 - 5432
--      SMTP - 192168001207 - 25
--      FTP - 192168001207 - 21
--      OracleSQL - 192168001207 - 1521)

-- ?
SELECT sc.name, serv.ip, p.number
FROM `Server` serv 
JOIN Port p ON serv.id = p.idServer
JOIN Service sc ON p.idService = sc.id

WHERE serv.idCampus = (
    SELECT p.idCampus 
    FROM Student s 
    JOIN Promo p ON s.idPromo = p.id 
    WHERE s.firstName = 'Jackson' AND s.lastName = 'Wilson')

ORDER BY serv.ip, p.number

//

SELECT sc.name, serv.ip, p.number 
FROM Student s 
JOIN Promo pr ON s.idPromo = pr.id 
JOIN `Server` serv ON serv.idCampus = pr.idCampus
JOIN Port p ON serv.id = p.idServer
JOIN Service sc ON p.idService = sc.id
WHERE s.firstName = 'Jackson' AND s.lastName = 'Wilson'

ORDER BY serv.ip, p.number



-- ! Requete 6:
-- Afficher la liste de tous les Macs (hormis les serveurs) et le nom, le program et le campus de leurs proprietaires

-- ?

SELECT c.id, CONCAT(s.firstName, " ", s.lastName) AS Name, pr.name, ca.name
FROM Computer c 
JOIN Loan l ON l.idComputer = c.id
JOIN Student s ON s.id = l.idStudent
JOIN Promo p ON p.id = s.idPromo
JOIN Campus ca ON ca.id = p.idCampus
JOIN Program pr ON pr.id = p.idProgram

WHERE c.os LIKE "Mac OS%" 
AND c.id NOT IN (SELECT idComputer FROM `Server`);




-- ! Requete 7:
-- Afficher les specs et les proprietaires de toutes les machines sous Windows, trié par ram et capacité mémoire

-- ?
SELECT CONCAT(s.firstName, " ", s.lastName) AS Name, m.constructor, m.number, m.pocesor, m.ram, m.hdd, IF(m.eth = 0, "Off", "On"), IF(m.wifi = 0, "Off", "On"), m.year, c.os
FROM Computer c 
JOIN Loan l ON l.idComputer = c.id
JOIN Student s ON s.id = l.idStudent
JOIN Model m ON m.id = c.idModel

WHERE c.os LIKE "Windows%" 
AND c.id NOT IN (SELECT idComputer FROM `Server`)
ORDER BY m.ram, m.hdd
LIMIT 0, 1000

-- ! Requete 8:
-- Afficher le nombre de machines sous windows, OSX et debian (hors serveurs), par campus

-- ?
SELECT ca.name, c.os, COUNT(*)
FROM Computer c 
JOIN Loan l ON l.idComputer = c.id
JOIN Student s ON s.id = l.idStudent
JOIN Promo p ON s.idPromo = p.id
JOIN Campus ca ON p.idCampus = ca.id

WHERE c.id NOT IN (SELECT idComputer FROM `Server`)

GROUP BY p.idCampus, c.os
LIMIT 0, 1000

-- ! Requete 9:
-- Reprenez la requete 7 et modifier la pour n'afficher les info que des personnes qui sont dans une classe ayant une majorité de machine Windows

-- ?

SELECT 
    s.idPromo, 
    CONCAT(s.firstName, " ", s.lastName) AS Name, 
    m.constructor, 
    m.number, 
    m.pocesor, 
    m.ram, 
    m.hdd, 
    IF(m.eth = 0, "Off", "On"), 
    IF(m.wifi = 0, "Off", "On"), 
    m.year, 
    c.os

FROM Computer c 

JOIN Loan l ON l.idComputer = c.id
JOIN Student s ON s.id = l.idStudent
JOIN Model m ON m.id = c.idModel

WHERE c.os LIKE "Windows%" 
AND c.id NOT IN (SELECT idComputer FROM `Server`)
AND s.idPromo IN (
    SELECT p3.id
    FROM Promo p3
    JOIN (
        SELECT s1.idPromo AS idP, COUNT(*) as "Count" 
        FROM Computer c1 
        JOIN Loan l1 ON l1.idComputer = c1.id 
        JOIN Student s1 ON s1.id = l1.idStudent 
        WHERE c1.os NOT LIKE "Windows%" 
        GROUP BY s1.idPromo
    ) nw ON nw.idP = p3.id

    JOIN (
        SELECT s2.idPromo AS idP, COUNT(*) as "Count" 
        FROM Computer c2 
        JOIN Loan l2 ON l2.idComputer = c2.id 
        JOIN Student s2 ON s2.id = l2.idStudent 
        WHERE c2.os LIKE "Windows%" 
        GROUP BY s2.idPromo
    ) w ON w.idP = p3.id

    WHERE w.Count > nw.Count)
ORDER BY s.idPromo, m.ram, m.hdd
LIMIT 0, 1000




















































-- Si tu vois ceci, tu a scrollé trop bas... 