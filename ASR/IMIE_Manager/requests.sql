-- Les requetes demandées portens sur la Base de donnée IMIE_ASR, modélisée pendant les précédents cours.
-- Vous trouverez dans le dosier "BDD", deux fichier SQL permettant l'instalation de cette dernière.
-- Dans le dossier "Merise", vous trouverez la sauvegarde MySQL Workbench ayant servie à la modélisation de cette base, ainsi qu'un schema au format PNG


-- Durant cette FOAD, vous deverez implementer les requetes demandées et les sauvegarder dans ce document.


-- ! Requete 1:
-- Afficher le nom, le prénom et la promo et l'année d'entré de tout les éleves, trié par année

-- ? 


-- ! Requete 2:
-- Afficher le nom, le prénom, la promo, l'année, le nom du programme, le nom du campus et le code postale du campus de chaques éleves
-- trié par campus (alphabetique), puis program (alphabetique), puis année

-- ?


-- ! Requete 3:
-- Afficher le nom, le prénom et le nom de promo complet* de tous les etudiants de caen, trié par anciennetée
-- *Le nom complet d'une promo est composé comme suit:
--      "campusName"-"programName"-"ancienetée"
--      Par exemple, pour Liam kelly, dans la promo DEV 2016 de caen , le resultat sera:
--      "Caen-DEV-3"
-- (Attention avec MySQL, la concatenation de chaines se fait via la fonction CONCAT() - cf: https://www.w3schools.com/sql/func_mysql_concat.asp)

-- ?


-- ! Requete 4:
-- Afficher le nom, le prénom et les adresses MAC (eth et wifi) de chaque etudiants

-- ?


-- ! Requete 5:
-- Afficher le nom, l'ip et le port des chaque services disponnible pour un etudiant, en fonction de son nom et prénom, trié par ip et par port
-- (Par exemple, pour Jackson Wilson, ne renvoyer que les services disponnible dans son campus, soit:
--      Apache - 192168001203 -80
--      IRC - 192168001203 - 6697
--      PostgreSQL - 192168001203 - 5432
--      MySQL - 192168001203 - 3306
--      SSH - 192168001203 - 22
--      FTP - 192168001203 - 21
--      OracleSQL - 192168001203 - 1521
--      Tomcat - 192168001207 - 8080
--      PostgreSQL - 192168001207 - 5432
--      SMTP - 192168001207 - 25
--      FTP - 192168001207 - 21
--      OracleSQL - 192168001207 - 1521)

-- ?


-- ! Requete 6:
-- Afficher la liste de tous les Macs (hormis les serveurs) et le nom, le program et le campus de leurs proprietaires

-- ?


-- ! Requete 7:
-- Afficher les specs et les proprietaires de toutes les machines sous Windows, trié par ram et capacité mémoire

-- ?


-- ! Requete 8:
-- Afficher le nombre de machines sous windows, OSX et debian (hors serveurs), par campus

-- ?


-- ! Requete 9:
-- Reprenez la requete 7 et modifier la pour n'afficher les info que des personnes qui sont dans une classe ayany une majorité de machine Windows

-- ?



-- <3 <3 <3 Ammusez vous bien <3 <3 <3



















































-- Si tu vois ceci, tu a scrollé trop bas... 