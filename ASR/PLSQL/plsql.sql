
-- 1
DELIMITER |

CREATE FUNCTION countStudentForOs(CampusName VARCHAR(255), OS VARCHAR(255)) RETURNS INT

BEGIN

    DECLARE total INT;

    SELECT count(s.id) INTO total
    FROM student s

    JOIN loan l ON s.id = l.idStudent
    JOIN computer co ON co.id = l.idComputer
    JOIN promo p ON p.id = s.idPromo
    JOIN campus ca ON p.idCampus = ca.id

    WHERE LOWER(ca.name) like CONCAT(LOWER(CampusName), '%')
    AND LOWER(co.os) like CONCAT(LOWER(OS), '%');


    RETURN total;

END |


DELIMITER ;


--- 2

DROP PROCEDURE cleanServers;
DELIMITER |

CREATE PROCEDURE cleanServers() 

BEGIN
    DECLARE loanID INT;
    DECLARE finished INT DEFAULT 0;
    DECLARE total INT;

    DECLARE serv CURSOR FOR SELECT l.id FROM loan l WHERE l.idComputer IN (SELECT s.idComputer FROM `server` s); 
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = 1;

    SET total = 0;
    OPEN serv;
    cursorLoop: LOOP

        FETCH serv INTO loanId;
        IF finished = 1 THEN
            LEAVE cursorLoop;
        END IF;

        DELETE FROM loan WHERE id = loanId;
        SET total = total + 1;

    END LOOP;

    CLOSE serv;

    SELECT CONCAT('Removed ', total, ' Loans');
END|

DELIMITER ;



-- 3

DROP TRIGGER IF EXISTS `filterOS`;
DELIMITER |
CREATE TRIGGER `filterOS` BEFORE INSERT ON `Computer` FOR EACH ROW 

BEGIN

	IF LOWER(NEW.os) NOT LIKE 'win%' 
    AND LOWER(NEW.os) NOT LIKE 'mac%' 
    AND LOWER(NEW.os) NOT LIKE 'debian%'
    THEN
    	INSERT INTO `INCORRECT OS` VALUES (1);
    END IF;

END|
DELIMITER ;

-- 4
DROP PROCEDURE `countStudents`;
DELIMITER |
CREATE PROCEDURE `countStudents`() BEGIN 
    
	DECLARE res TEXT;
	DECLARE campusName VARCHAR(255);
    DECLARE end_cursor INT DEFAULT 0;
    DECLARE campus_cursor CURSOR FOR SELECT name FROM Campus;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET end_cursor = 1; 


    SET res = CONCAT(RPAD("Campus", 25, " "), RPAD("OS", 25, " "), "Count", "\n");

	OPEN campus_cursor;
        campus_loop: LOOP
        
            FETCH campus_cursor INTO campusName;
            IF end_cursor = 1 THEN	
                LEAVE campus_loop;
            ELSE
                BLOCK2: BEGIN
                    DECLARE osName VARCHAR(255);
                    DECLARE c INT;
                    DECLARE end_os_cursor INT DEFAULT 0;
                    DECLARE os_cursor CURSOR FOR SELECT DISTINCT os FROM Computer;
                    DECLARE CONTINUE HANDLER FOR NOT FOUND SET end_os_cursor = 1; 
                    OPEN os_cursor;
                        os_loop: LOOP
                            FETCH os_cursor INTO osName;
                            IF end_os_cursor = 1 THEN	
                                LEAVE os_loop;
                            ELSE
                                SELECT countStudentForOs(campusName, osName) INTO c;
                                SET res = CONCAT(res, RPAD(CampusName, 25, " "), RPAD(osName, 25, " "), c, "\n");

                            END IF;
                        END LOOP os_loop;
                    CLOSE os_cursor;
                END BLOCK2;
            END IF;
        END LOOP campus_loop;
    CLOSE campus_cursor;

    SELECT res;
END|

DELIMITER ;
