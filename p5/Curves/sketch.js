const max = 200
const speed = 3
let cols = 15
let w, h
let a = 0
let curves = []
let increment
function setup() {
	createCanvas(windowHeight, windowHeight)
	w = width / cols
	h = height / cols
	increment = speed * (TWO_PI / 360)
}

function draw() {
	background(0)

	noFill()
	let x = w / 2
	let y = h / 2
	let s = w - 4
	let r = s / 2

	let xs = []
	let ys = []
	for (let i = 1; i < cols; i++) {
		stroke(255)
		let x1 = i * w + w / 2
		let y1 = i * h + h / 2
		let f = map(i, 1, cols-1, 1, 3)
		
		// Circle
		strokeWeight(1)
		// Top
		ellipse(x1, y, s)
		// Left
		ellipse(x, y1, s)

		// Points
		strokeWeight(8)
		let px = r * cos(a * f)
		let py = r * sin(a * f)
		point(x1 + px, y + py)
		point(x + px, y1 + py)

		stroke(255, 100)
		strokeWeight(1)
		// Lines
		line(x1 + px, 0, x1 + px, height)
		line(0, y1 + py, width, y1 + py)

		xs.push(x1 + px)
		ys.push(y1 + py)
	}

	stroke(255)
	strokeWeight(4)
	for (let i = 0; i < xs.length; i++) {
		const x = xs[i]
		if (curves[i] == undefined) { curves[i] = [] }
		for (let j = 0; j < ys.length; j++) {
			const y = ys[j]
			if (curves[i][j] == undefined) { curves[i][j] = [] }
			point(x, y)
			if (curves[i][j].length > max) {
				curves[i][j].splice(0,1)
			}
			curves[i][j].push({x, y})
			
		}
	}
	strokeWeight(1)
	for (let i = 0; i < curves.length; i++) {
		for (let j = 0; j < curves[i].length; j++) {
			let points = curves[i][j]
			beginShape()
			for (const p of points) {
				vertex(p.x, p.y)
			}
			endShape()
		}
	}


	a += increment
}
