let world;
let balls = [];
let i = 0;
function setup() {
	createCanvas(windowWidth, windowHeight);
	world = new World(0.2, 0.2);
	world.setEdges(0,0,width, height);
	for (let i = 0; i < 1500; i++) {
		let m = random(4, 10);
		let x = random(m , width - m);
		let y = random(m , height / 3);
		ball = new Body(x, y, m, world);
		ball.applyForce(createVector(random(-40, 40), random(-40, 40)));
		balls.push(ball);
	}

	let v = createVector(width/2, height/2);
	v.setMag(1);
	world.setGravity(v);
}

function draw() {
	background(0);	
	noStroke();
	fill(255);
	for (let b of balls) {
		b.render(balls);
	}

	let x = map(noise(i, 0), 0, 1, -1, 1);
	let y = map(noise(i, 5), 0, 1, -1, 1);
	i += 0.1;
	world.setGravity(createVector(x, y));

	// let v = createVector(mouseX - width/2, mouseY - height/2);
	// v.setMag(1);
	// world.setGravity(v);
}


function mousePressed() {
	let v = createVector(mouseX - width/2, mouseY - height/2);
	v.setMag(1);
	world.setGravity(v);
}
