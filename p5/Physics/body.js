class World {
    // Drag between 0 and 100 (0 - vacum, 100 - solid)
    constructor(hGravity, vGravity) {
        this.setGravity(createVector(hGravity,vGravity));
        this.edges = undefined;
    }
    
    setEdges(x, y, w, h) {
        this.edges = { left: x, top: y, right: x + w, bottom: y + h };
    }
    
    setGravity(vetor) {
        this.gravity = vetor.copy();
        this.gravity.mult(0.1);
    }
}
class Body {
    constructor(x, y, mass, world, label) {
        this.label = label;
        this.pos = createVector(x, y);
        this.vel = createVector();
        this.acc = createVector();
        this.mass = mass;
        this.world = world;
    }
    
    render(balls) {
        noStroke();
        fill(255);
        ellipse(this.pos.x, this.pos.y, this.mass, this.mass);
        if (this.label) {
            stroke(0);
            fill(0);
            textAlign(CENTER, CENTER);
            textSize(this.mass/2);
            text(this.label, this.pos.x - this.mass/2, this.pos.y - this.mass/2, this.mass, this.mass);
        }
        this.update();
        this.interact(balls);
    }
    
    update() {
        // Adding gravity
        let gravity = this.world.gravity.copy();
        gravity.mult(this.mass);
        this.applyForce(gravity);
        
        // Applying acceleration
        this.vel.add(this.acc);
        this.acc.mult(0);       
        
        // Computing new Position
        this.pos.add(this.vel);
        
        this.vel.mult(0.99);
        this.constrain();
    }
    
    applyForce(force) {
        this.acc.add(force);
    }
    
    constrain() {
        if (this.world.edges) {
            let r = this.mass / 2;
            let edges = this.world.edges;
            let top = this.pos.y - r;
            let bottom = this.pos.y + r;
            let left = this.pos.x - r;
            let right = this.pos.x + r;
            
            if (top <= edges.top || bottom >= edges.bottom) {
                this.vel.y *= -0.9; 
                if (top <= edges.top) { this.pos.y = edges.top + r; }
                else { this.pos.y = edges.bottom - r; }
            }
            
            if (left <= edges.left || right >= edges.right) {
                this.vel.x *= -0.9; 
                if (left <= edges.left) { this.pos.x = edges.left + r; }
                else { this.pos.x = edges.right - r; }
            }
            
            
        }
    }
    
    interact(bodies){
        for (let b of bodies) {
            if (b != this) {

                if (touching(this.pos.x, this.pos.y, this.mass/2, b.pos.x, b.pos.y, b.mass/2)) {
                    let m1 = this.mass;
                    let m2 = b.mass;
                    let u1 = this.vel.copy();
                    let u2 = b.vel.copy();

                    let t1 = (m1 - m2) / (m1 + m2);
                    u1.mult(t1);
                    let t2 = (2 * m2) / (m1 + m2);
                    u2.mult(t2);
                    u1.add(u2);
                    let v1 = u1.copy();

                    u1 = this.vel.copy();
                    u2 = b.vel.copy();
                    t1 = (m2 - m1) / (m1 + m2);
                    u2.mult(t1);
                    t2 = (2 * m1) / (m1 + m2);
                    u1.mult(t2);    
                    u1.add(u2);
                    let v2 = u1.copy();

                    v1.mult(0.99);
                    v2.mult(0.99);
                    this.vel = v1;
                    b.vel = v2;


                    let a = atan2(b.pos.y - this.pos.y, b.pos.x - this.pos.x);
                    let x = this.pos.x + (this.mass/2 * cos(a));
                    let y = this.pos.y + (this.mass/2 * sin(a));


                    let v = createVector(x - this.pos.x, y - this.pos.y);
                    v.rotate(PI);

                    this.pos.x = b.pos.x + ((b.mass/2 + this.mass/2) * cos(v.heading()));
                    this.pos.y = b.pos.y + ((b.mass/2 + this.mass/2) * sin(v.heading()));
                }
            }
        }
    }
}

function touching(x1, y1, r1, x2, y2, r2) {
    let a = x1 - x2;
    let b = y1 - y2;
    let d = a*a + b*b;
    let r = (r1 + r2) * (r1 + r2);
    return  (d <= r);
}