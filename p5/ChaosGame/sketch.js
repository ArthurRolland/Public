const points = 3
let bases = []
let last
let lerpX, lerpY
function setup() {
	createCanvas(windowWidth, windowHeight)
	background(255)
	angleMode(DEGREES)
	colorMode(HSB)
	translate(width / 2, height / 2)
	let radius = min(width, height) / 2

	lerpX = random(0.3, 0.7)
	lerpY = random(0.3, 0.7)
	lerpX = 0.5
	lerpY = 0.5


	for (let i = 0; i < points; i++) {
		let a = -90 + i * (360 / points)
		let c = map(i, 0, points - 1, 0, 255)
		bases.push({ x: radius * cos(a), y: radius * sin(a), c: c })
	}
	// bases.push({ x: r * cos(-90), y: r * sin(-90), r: 255, g: 0,   b: 0   })
	// bases.push({ x: r * cos(30),  y: r * sin(30),  r: 0,   g: 255, b: 0   })
	// bases.push({ x: r * cos(150), y: r * sin(150), r: 0,   g: 0  , b: 255 })

	// bases.push({ x: random(-width/2, width/2), y: random(-height/2, height/2), r: 255, g: 0,   b: 0   })
	// bases.push({ x: random(-width/2, width/2),  y: random(-height/2, height/2),  r: 0,   g: 255, b: 0   })
	// bases.push({ x: random(-width/2, width/2), y: random(-height/2, height/2), r: 0,   g: 0  , b: 255 })
	last = random(bases)
	strokeWeight(1)
	for (const p of bases) {
		stroke(p.c, 255, 255)
		point(p.x, p.y)
	}
}

function draw() {

	translate(width / 2, height / 2)
	for (let i = 0; i < 1000; i++) {
		let newR = random(bases)
		last = newPoint(last, newR)
		stroke(last.c, 255, 255)
		point(last.x, last.y)
	}
}

function newPoint(p1, p2) {
	return {
		x: lerp(p1.x, p2.x, lerpX),
		y: lerp(p1.y, p2.y, lerpY),
		c: lerp(p1.c, p2.c, 0.5)
	}
}
