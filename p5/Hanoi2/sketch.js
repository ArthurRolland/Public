const NUMBER_OF_BLOCKS = 11
let count = 0
const blockHeight = 30
let blockWidth
let margin

let p1, p2, p3

let from = null
let to = null

let lab



function setup() {
	createControls()
	createCanvas(1200, 600);
	let w = width / 3
	blockWidth = w * 0.9
	margin = w * 0.1
	setupGame()
	
}

function setupGame() {
	let p2Center = width / 2
	let p1Center = p2Center - width / 3
	let p3Center = p2Center + width / 3

	p1 = new Post("A", p1Center)
	p2 = new Post("B", p2Center)
	p3 = new Post("C", p3Center)

	let last = blockWidth
	for (let i = 1; i < NUMBER_OF_BLOCKS + 1; i++) {
		let size = map(i, 1, NUMBER_OF_BLOCKS, blockWidth, blockHeight)
		if (last - blockWidth < blockHeight) { size = last - blockHeight }
		last = size
		let r = map(i, 1, NUMBER_OF_BLOCKS, 0, 255)
		let g = 0
		let b = map(i, 1, NUMBER_OF_BLOCKS, 255, 0)
		let block = new Block(i, size, color(r, g, b))
		p1.add(block)
	}
}

function draw() {
	updateControls()
	background(57);

	noStroke()
	fill(255)

	let top = height - (3 * blockHeight) - margin / 2
	rect(margin / 2, top, width - margin, 3 * blockHeight)

	rect(p1.center - blockHeight / 2, height * 0.25, blockHeight, top - height * 0.25)
	rect(p2.center - blockHeight / 2, height * 0.25, blockHeight, top - height * 0.25)
	rect(p3.center - blockHeight / 2, height * 0.25, blockHeight, top - height * 0.25)

	stroke(0)
	p1.draw(top)
	p2.draw(top)
	p3.draw(top)
}

function move(from, to) {
	console.log("Moving " + from.name + " to " + to.name)
	let b = from.remove()
	if (b) { to.add(b) }
	count++
}

function hanoi(n, from, via, to) {
	if (n == 0) { return }
	hanoi(n -1, from, to, via)
	move(from, to)
	hanoi(n -1, via, from, to)
}

// Buttons and stuff
function createControls() {
	let div = createDiv()
	div.id("controls")
	lab = createP('From: <br/>To:')
	let buttonA = createButton("A")
	buttonA.mousePressed(buttonAAction)
	let buttonB = createButton("B")
	buttonB.mousePressed(buttonBAction)
	let buttonC = createButton("C")
	buttonC.mousePressed(buttonCAction)
	let button = createButton("Move")
	button.mousePressed(buttonAction)
	div.child(lab)
	div.child(buttonA)
	div.child(buttonB)
	div.child(buttonC)
	div.child(button)
}
function updateControls() {
	let s = "From: "
	if (from) { s += from.name }
	s += "<br/>To: "
	if (to) { s += to.name }
	lab.html(s)
}

function buttonAAction() {
	if (from) { to = p1 }
	else { from = p1 }
}
function buttonBAction() {
	if (from) { to = p2 }
	else { from = p2 }
}
function buttonCAction() {
	if (from) { to = p3 }
	else { from = p3 }
}

function buttonAction() {
	// if (from && to) { move(from, to) }
	// from = null
	// to = null


	hanoi(NUMBER_OF_BLOCKS, p1, p2, p3)
	
}


class Post {
	constructor(name, center) {
		this.name = name
		this.center = center
		this.blocks = []
	}
	add(block) {
		this.blocks.push(block)
	}
	remove() {
		return this.blocks.pop()
	}

	draw(bottom) {
		let y = bottom
		for (const b of this.blocks) {
			y = b.draw(this.center, y)
		}
	}
}

class Block {
	constructor(index, size, color) {
		this.index = index
		this.size = size 
		this.color = color
	}
	draw(centerX, bottom) {
		fill(this.color)
		let x = centerX - this.size / 2
		let y = bottom - blockHeight - 1
		rect(x, y, this.size, blockHeight)
		return y
	}

}


// function fact(x) {
// 	console.log("fact of" + x)
// 	if (x == 0) { return 1 }
// 	return x * fact(x - 1)
// }

// function crawl(folder) {
// 	let array = []
// 	for (const element of folder) {
// 		if (element.isFile()) {
// 			array.push(element)
// 		}
// 		else {
// 			array += crawl(element)
// 		}
// 	}
// 	return array
// }

// class Element {
// 	constructor(elements) {
// 		this.childs = elements
// 	}
// 	isFile() { return !this.childs }

// 	crawl() {
// 		if (this.isFile()) { return [this] }
// 		let array = []
// 		for (const e of this.element) { array += crawl.crawl() }
// 		return array
// 	}
// }