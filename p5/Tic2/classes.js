class Box {
    static EMPTY = -1
    static PLAYER1 = 0
    static PLAYER2 = 1

    static SYMBOLS = ["O", "X"]

    constructor(x,y, grid) {
        this.state = Box.EMPTY
        this.coords = { x, y }
        this.grid = grid
    }

    symbol() { 
        if (this.state >= 0 && this.state < Box.SYMBOLS.length ) {
            return Box.SYMBOLS[this.state]
        }
        return ""
    }

    x() { return this.coords.x * this.grid.sizeForCell() }
    y() { return this.coords.y * this.grid.sizeForCell() }

    detectClick(x, y) {
        let s = this.grid.sizeForCell()
        let boxX = this.x()
        let boxY = this.y()
        return (x >= boxX && x < boxX + s && y >= boxY && y < boxY + s)
    }

    draw() {
        let s = this.grid.sizeForCell()
        let x = this.x()
        let y = this.y()
        stroke(0)
        fill(255)
        rect(x, y, s, s)

        fill(0, 0, 255)
        if (this.state == Box.PLAYER1) { fill(0,255,0) }
        textAlign(CENTER, CENTER)
        textSize(s)
        text(this.symbol(), x + s /2, y + s /2 )
    }
}

class Grid {
    constructor() {
        this.boxes = []
        for (let x = 0; x < 3; x++) {
            for (let y = 0; y < 3; y++) {
                let b = new Box(x, y, this)
                this.boxes.push(b)
            }
        }
        this.currentPlayer = Box.PLAYER1
        this.winner = null
    }

    sizeForCell() {
        return min(width - 1, height - 1) / 3
    }

    draw() {
        for (const b of this.boxes) {
            b.draw()
        }
    }

    detectClick(x, y) {
        let box
        for (const b of this.boxes) {
            if (b.detectClick(x, y)) {
                box = b
                break
            }
        }
        if (!box) { return }
        this.play(box)
    }

    play(box) {
        if (box.state == Box.EMPTY) {
            box.state = this.currentPlayer
            let winner = this.gameOver(box)
            if (winner != null) { this.winner = winner }
            else { this.currentPlayer = (this.currentPlayer+1) %2 }
            
        }
        
    }

    boxForCoords(x, y) {
        for (const b of this.boxes) { if (b.coords.x == x & b.coords.y == y) { return b } }
        return undefined
    }

    gameOver(box) {
        let c = 0
        for (let x = 0; x < 3; x++) {
            if (this.boxForCoords(x, box.coords.y).state == this.currentPlayer) { c++ }
        }
        if (c == 3) { return this.currentPlayer }

        c = 0
        for (let y = 0; y < 3; y++) {
            if (this.boxForCoords(box.coords.x, y).state == this.currentPlayer) { c++ }
        }
        if (c == 3) { return this.currentPlayer }

        if (box.coords.x == 0 && box.coords.y == 0 || box.coords.x == 1 && box.coords.y == 1 || box.coords.x == 2 && box.coords.y == 2) { 
            c = 0
            for (let i = 0; i < 3; i++) {
                if (this.boxForCoords(i, i).state == this.currentPlayer) { c++ }
            }
            if (c == 3) { return this.currentPlayer }
        }
        
        if (box.coords.x == 0 && box.coords.y == 2 || box.coords.x == 1 && box.coords.y == 1 || box.coords.x == 2 && box.coords.y == 0) {
            c = 0
            for (let i = 0; i < 3; i++) {
                if (this.boxForCoords(i, 2-i).state == this.currentPlayer) { c++  }
            }
            if (c == 3) { return this.currentPlayer }
        }

        for (const b of this.boxes) {
            if (b.state == Box.EMPTY) { return null }
        }

        return Box.EMPTY
    }
}