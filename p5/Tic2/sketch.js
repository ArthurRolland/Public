let grid

function setup() {
	createCanvas(600, 600)
	grid = new Grid()
}

function draw() {
	background(0)
	
	grid.draw()
	if (grid.winner != null) {
		textAlign(CENTER)
		textSize(width / 6	)
		
		let t = "DRAW"
		fill(255, 0, 0)
		if (grid.winner == Box.PLAYER1) {
			t = "PLAYER 1"
			fill(0, 255, 0)
		}
		else if (grid.winner == Box.PLAYER2) {
			t = "PLAYER 2"
			fill(0, 0, 255)
		}
		text(t, width / 2, height / 2)
		noLoop()
	}
}

function mousePressed() {
	grid.detectClick(mouseX, mouseY)
}