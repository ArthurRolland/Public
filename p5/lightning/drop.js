class Drop {

    constructor() {
        this.pos = undefined;
        this.velocity = undefined;
        this.setup();
    }

    setup() {
        this.pos = createVector(random(-width/3, width), random(-height/2, 0), random(1, 5));
        this.velocity = createVector(random(0.1, 0.3), random(0.5, 1));
        this.update();
    }

    render() {
        let w = dropSize / this.pos.z;
        let h = w * 2;
        strokeWeight(w);
        let a = map(this.pos.z, 1, 5, 50, 200);
        stroke(0, 0, 255, a);
        line(this.pos.x, this.pos.y, this.pos.x, this.pos.y + h);
    }

    update() {
        if (this.pos.y > height) {
            this.setup();
        }
        else {
            let acc = this.velocity.copy();
            acc.mult(dropSpeed / this.pos.z);
            this.pos.add(acc);
        }
        
    }
}