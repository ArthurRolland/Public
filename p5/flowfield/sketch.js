let offset = 0
let cols = 50
let scl = 0.25
let speed = 0.125
let p
function setup() {
	createCanvas(windowHeight, windowHeight)
	p = createVector(width/2, height/2)
}

function draw() {
	background(57);
	let w = width / cols;
	let h = height / cols;
	let pi = max(min(floor(p.x / w), cols-1), 0)
	let pj = max(min(floor(p.y / h), cols-1), 0)
	stroke(255, 50)
	strokeWeight(1)
	noFill()
	for (let i = 0; i < cols; i++) {
		for (let j = 0; j < cols; j++) {
			let x = i * w + w / 2
			let y = j * h + h / 2
			// ellipse(x, y, w)
			let xOff = i * scl + offset
			let yOff = j * scl + offset
			let n = map(noise(xOff, yOff), 0, 1, 0, TWO_PI)
			let v = createVector(1,1)
			v.setMag(w / 2)
			v.rotate(n)
			line(x, y, x + v.x, y + v.y)
			if (i == pi && j == pj) {
				p.add(v)
			}
		}
	}
	strokeWeight(16)
	stroke(255, 0, 0)
	point(p.x, p.y)

	if (p.x > width) { p.x = 0 }
	else if (p.x < 0) { p.x = width }
	if (p.y > height) { p.y = 0 }
	else if (p.y < 0) { p.y = height }

	// offset += speed
}
