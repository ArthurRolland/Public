class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
}

class Size {
	constructor(width, height) {
		this.width = width;
		this.height = height;
	} 
}

class Shape {
	constructor(x, y, w, h) {
		this.origin = new Point(x, y);
		this.size = new Size(w, h);
	}
	render() {};
	center() { return new Point(this.origin.x + this.size.width / 2, this.origin.y + this.size.height / 2); }
	setCenter(point) { this.origin = new Point(point.x - this.size.width / 2, point.y - this.size.height / 2); }
	top() { return this.origin.y; }
	bottom() { return this.origin.y + this.size.height; }
	left() { return this.origin.x; }
	right() { return this.origin.x + this.size.width; }
	contains(point) { return (point.x >= this.left() && point.x < this.right() &&  point.y >= this.top() && point.y < this.bottom()); }

	intersects(other) {
		if (this.right() < other.left()) { return false; }
		if (this.left() > other.right()) { return false; }
		if (this.bottom() < other.top()) { return false; }
		if (this.top() > other.bottom()) { return false; }
		return true;
	}
}
class Rectangle extends Shape {
	render() { rect(this.origin.x, this.origin.y, this.size.width, this.size.height); }
}


class Circle extends Shape {
	render() { ellipse(this.center().x, this.center().y, this.size.width, this.size.height); }
}