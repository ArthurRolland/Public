let qTree;
let r;
let points = [];
let mouseIsPressed = false;

function setup() {
	createCanvas(600, 600);
	qTree = new QuadTree(new Rectangle(0, 0, width, height), 4);
	// for (let i = 0; i < 1000; i++) {
	// 	let p = new Point(random(width), random(height));
	// 	points.push(p);
	// 	qTree.insert(p);
	// }
	r = new Rectangle(0,0, 50, 50); 
}
function mousePressed() { mouseIsPressed = true; }
function mouseReleased() { mouseIsPressed = false; }

function addPoint() {
	let p = new Point(mouseX, mouseY);
	points.push(p);
	qTree.insert(p);
}

function draw() {
	background(57);
	if (mouseIsPressed) { 
		// r.setCenter(new Point(mouseX, mouseY)); 
		addPoint();
	}
	
	let found = qTree.query(r);

	noFill();
	stroke(255);
	strokeWeight(1);
	qTree.render();

	stroke(255);
	strokeWeight(2);
	for (let p of points) {
		point(p.x, p.y);
	}

	stroke(0,255,0);
	strokeWeight(1);
	r.render();

	strokeWeight(3);
	for (let p of found) {
		point(p.x, p.y);
	}
}


function keyPressed() {
	if (key == " ") {
		r.size.width += 5;
		r.size.height += 5;
	}
}