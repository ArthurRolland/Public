const QTREE_NORTH_WEST = 0;
const QTREE_NORTH_EAST = 1;
const QTREE_SOUTH_EAST = 2;
const QTREE_SOUTH_WEST = 3;

class QuadTree {
    constructor(frame, maxPoints) {
        this.frame = frame;
        this.maxPoints = maxPoints;
        this.points = [];
        this.childs = null;
    }
    insert(point) {
        if (this.frame.contains(point)) {
            if (this.points.length < this.maxPoints) { this.points.push(point); }
            else {
                if (!this.childs) { this.divide(); }
                for (let c of this.childs) { c.insert(point); }
            }
        }
    }
    divide() {
        let w = this.frame.size.width / 2;
        let h = this.frame.size.height / 2;
        let x = this.frame.origin.x;
        let y = this.frame.origin.y;
        let nw = new Rectangle(x, y, w, h);
        let ne = new Rectangle(x + w, y, w, h);
        let se = new Rectangle(x + w, y + h, w, h);
        let sw = new Rectangle(x, y + h, w, h);
        this.childs = [new QuadTree(nw, this.maxPoints), new QuadTree(ne, this.maxPoints), new QuadTree(se, this.maxPoints), new QuadTree(sw, this.maxPoints)]
    }

    query(bounds) {
        let points = [];
        if (this.frame.intersects(bounds)) {
            for (let p of this.points) {
                if (bounds.contains(p)) { points.push(p); }
            }
            if (this.childs) { for (let c of this.childs) { points = [...points, ...c.query(bounds)]; } }
        }
        
        return points;
    }

    render() {
        this.frame.render();
        if (this.childs) { 
            for (let c of this.childs) { c.render(); } 
        }
    }
}