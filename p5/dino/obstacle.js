class Obstacle extends Rectangle {

    constructor(x, y, w, h, offset) {
        super(x, y, w, h);
        this.offset = offset;
        this.reset();
    }

    reset() {
        this.pos.x = width + random(-100, 100) + this.offset;
        this.velocity.x = obstacleSpeed * -1;
    }

    update() {
        super.update();
    }

    isOffscreen() {
        return this.pos.x + this.w < 0;
    }

}