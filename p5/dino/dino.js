class Dino extends Rectangle {

    constructor(pic) {
        let ratio =  pic.width/ pic.height;
        super(50, 50 * ratio, dinoSize, dinoSize);
        this.pic = pic;
    }   

    render() {
        image(this.pic, this.pos.x, this.pos.y, this.w, this.h);
    }

    touchTheFloor() {
        return this.pos.y + this.h == groundY; 
    }
}