class Rectangle {

    constructor(x, y, w, h) {
        this.w = w;
        this.h = h;
        this.pos = createVector(x, y);
        this.velocity = createVector(0,0);
        this.acceleration = createVector(0,0);
    }

    render() {
        rect(this.pos.x, this.pos.y, this.w, this.h);
    }

    update(){
        this.velocity.add(this.acceleration);
        this.acceleration.mult(0);
        this.pos.add(this.velocity);

        if (this.pos.y + this.h >= groundY) { 
            this.pos.y =  groundY - this.h;
            if (this.velocity.y > 0) {
                this.velocity.y = 0;
            }
        }
    }

    applyForce(force) { this.acceleration.add(force); }

    private_intersect(other) {
        let x = min(this.pos.x, other.pos.x);
        let y = min(this.pos.y, other.pos.y);

        let xx = max(this.pos.x + this.w, other.pos.x + other.w);
        let yy = max(this.pos.y + this.h, other.pos.y + other.w);

        let w = xx - x;
        let h = yy - y;

        return !(this.w + other.w <= w || this.h + other.h <= h);
    }

    intersect(other) {
        return this.private_intersect(other) || other.private_intersect(this);
    }
}