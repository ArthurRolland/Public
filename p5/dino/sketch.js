const minSpeed = 6;
const maxSpeed = 15;
let obstacleSpeed;
let dinoPic;

let isStopped = false;
const dinoSize = 90;
const maxJumpCount = 7;
let groundY;
let gravity;
let score = 0;

let dino;
let speedWatcher;
let obstacles = [];
let jumpSettings = { isPressed: false, frameCount: 0, couldJump: true };

function preload() {
	dinoPic = loadImage('dino2.png');
}


function setup() {
	createCanvas(windowWidth, windowHeight);
	obstacleSpeed = minSpeed;
	groundY = height * 0.75;
	gravity = createVector(0, 1.5);

	

	dino = new Dino(dinoPic);
	speedWatcher = new Obstacle(0, groundY, 10, 10);
	let chunk = width / 4;
	obstacles.push(new Obstacle(0, groundY, dinoSize * 0.27, dinoSize * 0.75, 0));
	obstacles.push(new Obstacle(0, height / 6, dinoSize * 0.7, dinoSize * 0.5, chunk));
	obstacles.push(new Obstacle(0, groundY, dinoSize * 0.27, dinoSize * 0.75, chunk * 2));
	obstacles.push(new Obstacle(0,  height / 6	, dinoSize * 0.7, dinoSize * 0.5, chunk * 3));
	
	
}


function reset() {
	obstacleSpeed = minSpeed;
	score = 0;
	for (o of obstacles) {
		o.reset();
	}
}

function draw() {
	background(255);
	stroke(0);
	line(0, groundY, width, groundY);
	textSize(50);
	fill(0);
	text("Score: " + score, width - 300, 0, 300, 100);

	if (jumpSettings.isPressed && jumpSettings.frameCount < maxJumpCount) {
		dino.applyForce(createVector(0, -4.5));
		jumpSettings.frameCount++;
	}

	
	if (!dino.touchTheFloor()) { dino.applyForce(gravity); }
	else { jumpSettings.couldJump = true;  }
	
	dino.update();
	speedWatcher.update();
	if (speedWatcher.isOffscreen()) {
		obstacleSpeed *= 1.1;
		obstacleSpeed = constrain(obstacleSpeed, minSpeed, maxSpeed);
		speedWatcher.reset()
	}

	noStroke();
	
	dino.render();
	fill(0, 255, 0);
	for (o of obstacles) {
		o.update();
		if (o.isOffscreen()) { o.reset(); }
		o.render();
		if (dino.intersect(o)) {
			noLoop();
			isStopped = true;
			textSize(100);
			text("LOOSER!!!", width/2 - 250, height/2 - 250, 500, 500);
		}
	}
	if (frameCount % 6 == 0){score++;}
}


function keyPressed() {
	if (isStopped) {
		isStopped = false;
		reset();
		loop();
	}
	if (jumpSettings.couldJump) { 
		
		if (key == " ") {
			jumpSettings = { isPressed: true, frameCount: 0, couldJump: false }
		}
	}
	
}

function keyReleased() {
	if (key == " ") {
		jumpSettings.isPressed = false;
	}
}
