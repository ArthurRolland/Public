class Bolt {
    
    constructor(parent) {
        this.points = [];
        this.childs = [];
        this.splitChances = 1;
        this.reached = false;

        if (parent) {
            this.parent = parent;
            this.growthFactor = this.parent.growthFactor * 0.9;
            this.parent.addChild(this);
            this.points.push(this.parent.end().copy());
        }
        else {
            this.parent = undefined;
            this.growthFactor = growthFactor;
            let w = width / 4;
            this.points.push(createVector(random(w, w*3), 0));
        }
        
    }

    start() { return this.points[0]; }
    end() { return this.points[this.points.length - 1]; }
    addChild(child) { this.childs.push(child); }
    removeChild(child) { this.childs.splice(this.child.indexOf(child), 1); }
    
    setIsEnded() {
        this.reached = true;
        if (this.parent) {
            this.parent.setIsEnded();
        }
    }
    

    render() {
        for (let i = 0; i < this.points.length; i++){
            let w = this.reached ? 6 : 2;
            strokeWeight(w);
            stroke(255);
            if (i > 0) {
                let p1 = this.points[i];
                let p2 = this.points[i - 1];
                line(p1.x, p1.y, p2.x, p2.y);
            }
        }

        for (let i = 0; i < this.childs.length; i++) {
            this.childs[i].render();
        }
    }

    grow() {
        if (this.childs.length > 0) {
            for (let i = 0; i < this.childs.length; i++) {
                this.childs[i].grow();
            }
        }
        else {
            let dir = createVector(random(-1, 1), random(0.25, 1));
            let gf = random(this.growthFactor/2, this.growthFactor)
            dir.mult(gf);
            let last = this.end().copy();
            last.add(dir);
            this.points.push(last);
            if (last.y > height) {
                this.setIsEnded();
            }
            else {
                this.split();
            }
            
        }
    }

    split() {
        let chance =  random(0, luckFactor);
        if (chance < this.splitChances) {
            let childsCount = floor(random(1,4));
            for (let i = 0; i <= childsCount; i++) {
                let b = new Bolt(this);
            }
        }
        else { this.splitChances += 1; }
    }

}