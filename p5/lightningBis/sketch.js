const growthFactor = 15;
const luckFactor = 500;
const fr = 100;
let bolt;

function setup() {
	frameRate(fr);
	createCanvas(400, 400);
	bolt = new Bolt();
}
function draw() {
	background(0);
	bolt.render();
	if(bolt.reached) {
		// background(255);
	}
	else {
		bolt.grow();
	}
	
}
