const charSize = 24;

let letters = [];
// let f;
function preload() {
	f = loadFont('Boku2-Regular.otf');
}
function setup() {
	let canvas = createCanvas(windowWidth, windowHeight);
	canvas.parent("#michel");
	for (let i = 0; i < width / charSize; i++) {
		let offset = random(15, 600);
		let speed = random(2, 15);
		let size = charSize;
		let z = random(-5, 5);
		for (let j = 0; j < round(random(15, 85)); j++) {
			let white = (j == 0 && random(1) < 0.3);
			let l = new Letter(i * charSize, j * -size, z, speed, size, offset, white);
			letters.push(l);
		}
	}
}

function draw() {
	background(0, 0, 0, 75);
	for(l of letters) {
		l.render();
	} 
}