class Letter {
    constructor(x, y, z, speed, size, offset, white) {
        this.char = this.randomChar();
        this.offset = offset;
        this.pos = createVector(x, y - this.offset, z);
        this.speed = speed;
        this.isWhite = white;
        this.size = size;
    }

    randomChar() {
        let i = round(random(12353, 12543));
        return char(i);
    }

    render() {
        this.pos.y += this.speed;
        if (this.pos.y > height) { this.pos.y = - this.offset;}
        if (random(1) < 0.05) { this.char = this.randomChar(); }


        noStroke();
        // textFont(f);
        textSize(this.size);
        fill(255, 55);
        text(this.char, this.pos.x, this.pos.y, this.pos.z + 3);
        let c = color(55, 255, 55);
        if (this.isWhite) {
            c = color(255, 255, 255)
        }
        textSize(this.size*0.75);
        fill(c);
        text(this.char, this.pos.x, this.pos.y, this.pos.z);
    }

}