const scl = 100
let a = 0
let shape;
let cXY, cXZ, cXW, cYZ, cYW, cZW, cX, cY, cZ;
let aXY = 0;
let aXZ = 0;
let aXW = 0;
let aYZ = 0;
let aYW = 0;
let aZW = 0;
let aX = 0;
let aY = 0;
let aZ = 0;

function setup() {
	createCanvas(windowWidth, windowHeight - 100)

	cXY = createCheckbox('XY', false)
	cXZ = createCheckbox('XZ', false)
	cXW = createCheckbox('XW', true)
	cYZ = createCheckbox('YZ', false)
	cYW = createCheckbox('YW', false)
	cZW = createCheckbox('ZW', false)
	cX = createCheckbox('X', false)
	cY = createCheckbox('Y', false)
	cZ = createCheckbox('Z', false)

	shape = make4d()
}

function move() {
	let points = shape

	let xy = rotation4dXY(aXY)
	let xz = rotation4dXZ(aXZ)
	let xw = rotation4dXW(aXW)
	let yz = rotation4dYZ(aYZ)
	let yw = rotation4dYW(aYW)
	let zw = rotation4dZW(aZW)

	let matrices4d = [xy, xz, xw, yz, yw, zw]
	// if (cXY.checked()) { matrices4d.push(xy) }
	// if (cXZ.checked()) { matrices4d.push(xz) }
	// if (cXW.checked()) { matrices4d.push(xw) }
	// if (cYZ.checked()) { matrices4d.push(yz) }
	// if (cYW.checked()) { matrices4d.push(yw) }
	// if (cZW.checked()) { matrices4d.push(zw) }

	points = apply4dTransforms(points, matrices4d)

	let x = rotation3dX(aX)
	let y = rotation3dY(aY)
	let z = rotation3dZ(aZ)

	let matrices3d = [x, y, z]
	// if (cX.checked()) { matrices4d.push(x) }
	// if (cY.checked()) { matrices4d.push(y) }
	// if (cZ.checked()) { matrices4d.push(z) }

	points = apply3dTransforms(points, matrices3d)

	if (cXY.checked()) { aXY += 0.01 }
	if (cXZ.checked()) { aXZ += 0.01 }
	if (cXW.checked()) { aXW += 0.01 }
	if (cYZ.checked()) { aYZ += 0.01 }
	if (cYW.checked()) { aYW += 0.01 }
	if (cZW.checked()) { aZW += 0.01 }
	if (cX.checked()) { aX += 0.01 }
	if (cY.checked()) { aY += 0.01 }
	if (cZ.checked()) { aZ += 0.01 }
	// a += 0.01;
	return points
}

function draw() {
	let points = move()
	background(0)
	translate(width / 2, height / 2)

	drawCube(points)
}

// Draws
function drawCube(points) {
	makePoints(points)
	makeSquare(points, 0, 1)
	makeSquare(points, 4, 1)
	makeSquare(points, 8, 1)
	makeSquare(points, 12, 1)
	makeSquare(points, 0, 4)
	makeSquare(points, 8, 4)
	makeSquare(points, 0, 8)
	makeSquare(points, 4, 8)
}

function makeSquare(points, offset, step) {
	for (let i = 0; i < 4; i++) {
		let i1 = offset + i
		let i2 = (step != 1 || i < 3) ? offset + i + step : offset
		let p1 = points[i1] 
		let p2 = points[i2]
		makeLine(p1, p2)
	}
}
function makeLine(p1, p2) { line(p1[0] * scl, p1[1] * scl, p2[0] * scl, p2[1] * scl) }

function makePoints(points) { points.forEach((p, index) => { makePoint(p, index) }); }
function makePoint(p, i) { 
	stroke(255)
	fill(255)
	let x = p[0] * scl
	let y = p[1] * scl
	let s = scl / 8
	ellipse(x, y, s) 
	// fill(255, 0, 0)
	// text(i, x - s / 2, y - s / 2, s, s)
	// fill(255)
}