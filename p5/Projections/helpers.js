// Projections
const projection3dTo2d = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
function projection4dTo3d(points) { 
    let newPoints = []
	for (let p of points) {
        let distance = 2;
        let w = 1 / (distance - p[3]);
        const projection = [[w, 0, 0, 0], [0, w, 0, 0], [0, 0, w, 0], [0, 0, 0, 0]];
        let newP = mult(p, projection)
		newPoints.push(newP)
    }
	return newPoints 
}

// Rotations
// - 2d
function rotation2d(angle) { return [[cos(angle), -sin(angle)], [sin(angle), cos(angle)]] }

// - 3d
function rotation3dX(angle) { return [[1, 0, 0], [0, cos(angle), -sin(angle)], [0, sin(angle), cos(angle)]] }
function rotation3dY(angle) { return [[cos(angle), 0, -sin(angle)], [0, 1, 0], [sin(angle), 0, cos(angle)]] }
function rotation3dZ(angle) { return [[cos(angle), -sin(angle), 0], [sin(angle), cos(angle), 0], [0, 0, 1]] }

// - 4d
function rotation4dXY(angle) { return [[cos(angle), -sin(angle), 0, 0], [sin(angle), cos(angle), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]] }
function rotation4dXZ(angle) { return [[cos(angle), 0, -sin(angle), 0], [0, 1, 0, 0], [sin(angle), 0, cos(angle), 0], [0, 0, 0, 1]] }
function rotation4dXW(angle) { return [[cos(angle), 0, 0 ,-sin(angle)], [0, 1, 0, 0], [0, 0, 1, 0], [sin(angle), 0, 0, cos(angle)]] }
function rotation4dYZ(angle) { return [[1, 0, 0, 0], [0, cos(angle), -sin(angle), 0], [0, sin(angle), cos(angle), 0], [0, 0, 0, 1]] }
function rotation4dYW(angle) { return [[1, 0, 0, 0], [0, cos(angle), 0, -sin(angle)], [0, 0, 1, 0], [0, sin(angle), 0, cos(angle)]] }
function rotation4dZW(angle) { return [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, cos(angle), -sin(angle)], [0, 0, sin(angle), cos(angle)]] }


// Shapes
function make2d() { return [[-1, -1], [1, -1], [1, 1], [-1, 1]] }
function make3d() { return [[-1, -1, -1], [1, -1, -1], [1, 1, -1], [-1, 1, -1], [-1, -1, 1], [1, -1, 1], [1, 1, 1], [-1, 1, 1]] }
function make4d() { return [[-1, -1, -1, -1], [1, -1, -1, -1], [1, 1, -1, -1], [-1, 1, -1, -1], [-1, -1, 1, -1], [1, -1, 1, -1], [1, 1, 1, -1], [-1, 1, 1, -1], [-1, -1, -1, 1], [1, -1, -1, 1], [1, 1, -1, 1], [-1, 1, -1, 1], [-1, -1, 1, 1], [1, -1, 1, 1], [1, 1, 1, 1], [-1, 1, 1, 1]] }




// Transform
function apply4dTransforms(points, matrices) {
    let newPoints = applyRotations(points, matrices)
    newPoints = projection4dTo3d(newPoints)
    return newPoints
}

function apply3dTransforms(points, matrices) {
    let newPoints = applyRotations(points, matrices)
    newPoints = applyTransform(newPoints, projection3dTo2d)
    return newPoints
}

function applyRotations(points, matrices) {
    let newPoints = points;
    for (let matrix of matrices) {
        newPoints = applyTransform(newPoints, matrix)

    }
    return newPoints
}
function applyTransform(points, matrix) {
    let newPoints = []
	for (let p of points) {
        let newP = mult(p, matrix)
		newPoints.push(newP)
	}
	return newPoints
}


// Matrix math
function mult(point, matrix) {
    let res = []
    for (let i = 0; i < matrix.length; i++) {
        let l = matrix[i]
        let sum = 0
        for (let j = 0; j < point.length; j++) {
            let p = point[j]
            let v = l[j]
            if (!v) { v = 0 }
            sum += p * v
        }
        res.push(sum)
    }
    return res
}