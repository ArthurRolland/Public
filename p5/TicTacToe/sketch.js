const rows = 3
const cols = 3
let cells = []
let isPlayer1 = true
let head

let resetPending = false

function setup() {
	noLoop()
	head = createP('Current: Player 1')
	createCanvas(600, 600);
	for (let i = 0; i < cols; i++) {
		for (let j = 0; j < rows; j++) {
			cells.push(new Cell(j, i))
		}
	}
}

function draw() {
	background(255);
	for (const cell of cells) {
		cell.display()
	}
}

function mousePressed() {
	if (resetPending) {
		reset()
		return
	}
	let selected
	for (const cell of cells) {
		if (cell.containPoint(mouseX, mouseY)) {
			selected = cell
			break
		}
	}

	if (selected && selected.value == 0) {
		selected.value = isPlayer1 ? 1 : 2
		update()
	}
}

function reset() {
	resetPending = false
	for (const cell of cells) {
		cell.reset()
	}
	head.html('Current: ' + (isPlayer1 ? 'Player 1' : 'Player 2'))
	redraw()
}

function update() {
	const res = winner()
	let suplement = ''
	if (res.winner == 0) {
		// Game Over
		suplement = ' - Game Over - No winner'
		resetPending = true
	}
	else if (res.winner == null) {
		// Normal move
		isPlayer1 = !isPlayer1
	}
	else {
		resetPending = true
		for (const cell of res.cells) {
			cell.win = true
			suplement = ' - ' + (res.winner == 1 ? 'Player 1' : 'Player 2') + ' Win!'
		}
	}

	
	head.html('Current: ' + (isPlayer1 ? 'Player 1' : 'Player 2') + suplement)
	redraw()
}
// -> { winner: , cells: }
function winner() {
	let wCells = []
	let winner = null

	// Lines
	for (let j = 0; j < rows; j++) {
		let current = null
		wCells = []
		for (let i = 0; i < cols; i++) {
			let cell = cellFor(i, j)
			if (cell.value == 0 || current && cell.value != current) {
				wCells = []
				current = null
				break
			}
			else {
				current = cell.value
				wCells.push(cell)
			} 
		}
		if (current) {
			winner = current
			break
		}
	}

	if (!winner) {
		// Columns
		for (let i = 0; i < cols; i++) {
			let current = null
			wCells = []
			for (let j = 0; j < rows; j++) {
				let cell = cellFor(i, j)
				if (cell.value == 0 || current && cell.value != current) {
					wCells = []
					current = null
					break
				}
				else {
					current = cell.value
					wCells.push(cell)
				} 
			}
			if (current) {
				winner = current
				break
			}
		}
	}



	if (!winner && rows == cols) {
		// Diagonals

		// TL -> BR
		let current = null
		wCells = []
		for (let x = 0; x < cols; x++) {
			let cell = cellFor(x, x)
			if (cell.value == 0 || current && cell.value != current) {
				wCells = []
				current = null
				break
			}
			else {
				current = cell.value
				wCells.push(cell)
			} 
		}
		if (!current) {
			// TR -> BL
			wCells = []
			for (let x = 0; x < cols; x++) {
				let cell = cellFor(x, cols - x - 1)
				if (cell.value == 0 || current && cell.value != current) {
					wCells = []
					current = null
					break
				}
				else {
					current = cell.value
					wCells.push(cell)
				} 
			}	
		}

		if (current) {
			winner = current
		}
		
	}

	if (!winner) {
		// Game Over
		winner = 0
		for (const cell of cells) {
			if (cell.value == 0) {
				winner = null
				break
			}
		}
	}

	return { winner: winner, cells: wCells }
}

// Cell delegate
function frameForCellAt(row, col) {
	const w = width / cols
	const h = height / rows
	return {
		x: w * col,
		y: h * row,
		w: w,
		h: h
	}
}

function cellFor(row, col) {
	for (const cell of cells) {
		if (cell.row == row && cell.col == col) { return cell }
	}
	return null
}