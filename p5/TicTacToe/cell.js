class Cell {
    constructor(row, col) {
        this.row = row
        this.col = col
        this.value = 0
        this.win = false
    }

    display() {
        const frame = frameForCellAt(this.row, this.col)
        const offsetX = frame.w * 0.1
        const offsetY = frame.h * 0.1
        
        push()
        translate(frame.x, frame.y)

        stroke(0)
        strokeWeight(2)
        
        rect(0, 0, frame.w, frame.h)
        if (this.win) {
            stroke(0, 255, 0)
            strokeWeight(4)
        }
        if (this.value == 1) {
            // draw X
            line(offsetX, offsetY, frame.w - offsetX, frame.h - offsetY)
            line(frame.w - offsetX, offsetY, offsetX, frame.h - offsetY)
        } 
        else if (this.value == 2) {
            // draw O
            ellipse(frame.w / 2, frame.h / 2, frame.w * 0.8, frame.h * 0.8)
        }
        pop()
    }

    reset() {
        this.win = false
        this.value = 0
    }

    containPoint(x, y) {
        const frame = frameForCellAt(this.row, this.col)
        return (x >= frame.x && x <= frame.x + frame.w && y >= frame.y && y <= frame.y + frame.h)
    }
}