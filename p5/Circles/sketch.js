let offset = 0;
let moveAngle = 0;
const maxSize = 500;
function setup() {
	createCanvas(windowWidth, windowHeight);
}

function draw() {
	angleMode(DEGREES);
	colorMode(HSB);
	background(0,0,0);

	stroke(255, 0, 255);


	let mx = map(cos(moveAngle), -1, 1, 0, 1);
	let my = map(sin(moveAngle), -1, 1, 0, 1);
	let px = map(noise(mx, my, 0), 0, 1, maxSize, width-maxSize);
	let py = map(noise(mx, my, 10), 0, 1, maxSize, height-maxSize);
	
	line(0, 0, px, py);
	line(width, 0, px, py);
	line(0, height, px, py);
	line(width, height, px, py);
	

	translate(px, py);

	
	let z = 0;
	for(let i = 0; i < 5; i++) {
		let cx = map(cos(moveAngle), -1, 1, 0, 1);
		let cy = map(sin(moveAngle), -1, 1, 0, 1);
		let c = map(noise(cx, cy, 100*i), 0, 1, 0, 255);
		fill(c, 255, 255, 50);
		beginShape();
		for (let a = 0; a <= 360; a += 1){
			let x = map(cos(a), -1, 1, 0, 1) + offset;
			let y = map(sin(a), -1, 1, 0, 1) + offset;
	
			let r = map(noise(x, y, z), 0, 1, 10, maxSize - (i*100));
			x = r * cos(a);
			y = r * sin(a);
			vertex(x, y);
		}
		endShape();
		z += 0.1;
	}
	
	// let cx = map(cos(moveAngle), -1, 1, 0, 1);
	// let cy = map(sin(moveAngle), -1, 1, 0, 1);
	// let c1 = map(noise(cx, cy, 0), 0, 1, 0, 255);
	// fill(c1, 255, 255, 50);
	// beginShape();
	// for (let a = 0; a <= 360; a += 1){
		
	// 	let x = map(cos(a), -1, 1, 0, 1) + offset;
	// 	let y = map(sin(a), -1, 1, 0, 1) + offset;

	// 	let r = map(noise(x, y, z), 0, 1, 10, 300);
	// 	x = r * cos(a);
	// 	y = r * sin(a);
	// 	vertex(x, y);
	// }
	// endShape();

	// let c2 = map(noise(mx, my, 100), 0, 1, 0, 255);
	// fill(c2, 255, 255, 50);
	// beginShape();
	// z += 0.1;
	// for (let a = 0; a <= 360; a += 1){
		
	// 	let x = map(cos(a), -1, 1, 0, 1) + offset;
	// 	let y = map(sin(a), -1, 1, 0, 1) + offset;

	// 	let r = map(noise(x, y, z), 0, 1, 10, 200);
	// 	x = r * cos(a);
	// 	y = r * sin(a);
	// 	vertex(x, y);
	// }
	// endShape();
	// let c3 = map(noise(mx, my, 200), 0, 1, 0, 255);
	// fill(c3, 255, 255, 50);
	// beginShape();
	// z += 0.1;
	// for (let a = 0; a <= 360; a += 1){
		
	// 	let x = map(cos(a), -1, 1, 0, 1) + offset;
	// 	let y = map(sin(a), -1, 1, 0, 1) + offset;

	// 	let r = map(noise(x, y, z), 0, 1, 10, 100);
	// 	x = r * cos(a);
	// 	y = r * sin(a);
	// 	vertex(x, y);
	// }
	// endShape();
	offset += 0.005;
	
	point(0,0);
	moveAngle += 1;
}
