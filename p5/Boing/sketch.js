const gravityFactor = 0.5;
const paddleStep = 5;
let ball;
let paddle;
let gravity;

let direction = 0;

function setup() {
	createCanvas(600, 600);
	gravity = createVector(0, gravityFactor);
	ball = {
		pos: createVector(width / 2, 0),
		vel: createVector(),
		acc: createVector(),
		r: 16,
		update: function(paddle) {
			this.applyForce(gravity);
			let left = paddle.left();
			let right = paddle.right();
			if (this.pos.x >= left.x && this.pos.x <= right.x){
				if (paddle.pos.y - this.pos.y <= this.r * 0.5) {
					this.applyForce(createVector(random(-1,1), -25));
					this.pos.y = paddle.pos.y - this.r * 0.5;
				}
			}

			this.vel.add(this.acc);
			this.pos.add(this.vel);
			this.vel.limit(25);
			this.acc.mult(0);
			this.offscreen();
		},

		applyForce: function(force) {
			this.acc.add(force);
		},

		offscreen: function() {  
			if (this.pos.y + this.r * 0.5 > height) { this.pos.y = this.r * -0.5; }
			if (this.pos.x  <= this.r * 0.5) {
				this.vel.x *= -1; 
				this.pos.x =this.r * 0.5;
			}
			else if (this.pos.x + this.r * 0.5 >= width) { 
				this.vel.x *= -1; 
				this.pos.x = width - this.r * 0.5;
			}
		},

	};

	paddle = {
		pos: createVector(width / 2, height - 50),
		size: 50,
		left: function(){ return createVector(paddle.pos.x - paddle.size * 0.5, paddle.pos.y); },
		right: function(){ return createVector(paddle.pos.x + paddle.size * 0.5, paddle.pos.y); },
		move: function(direction) { this.pos.add(direction); }
	}
}

function draw() {
	background(57);
	angleMode(DEGREES);
	stroke(255);

	paddle.pos.x += direction * paddleStep;
	strokeWeight(4);
	let left = paddle.left();
	let right = paddle.right();
	line(left.x, left.y, right.x, right.y);


	strokeWeight(ball.r);
	ball.update(paddle);
	point(ball.pos.x, ball.pos.y);
	


}

function keyPressed(){
	if (key == "ArrowLeft"){
		direction = -1;
	} 
	else if(key == "ArrowRight"){
		direction = 1;
	} 
}

function keyReleased(){
	if (key == "ArrowLeft" || key == "ArrowRight") {
		direction = 0;
	}
}