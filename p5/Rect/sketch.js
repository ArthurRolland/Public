let rect1;
let rect2;
let mouseIsPressed = false;

function setup() {
	createCanvas(600, 600);
	rect1 = new Rectangle(300, 300, 100, 100);
	rect2 = new Rectangle(0, 0, 300, 300);
}



function mousePressed() { mouseIsPressed = true; }

function mouseReleased() { mouseIsPressed = false; }

function draw() {
	background(57);
	

	if (mouseIsPressed) { rect2.setCenter(new Point(mouseX, mouseY)); }
	
	let c1 = rect1.center();
	let c2 = rect2.center();
	noStroke();
	fill(255);
	textAlign(CENTER, CENTER);
	textSize(64);
	text("1", c1.x, c1.y);
	text("2", c2.x, c2.y);
	
	noFill();
	stroke(255);
	

	rect1.render();

	if (rect1.intersects(rect2)) {
		stroke(0, 255, 0);
	}
	else {
		stroke(255, 0, 0);
	}
	rect2.render();
}
