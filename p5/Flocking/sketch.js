let boids = []
let sAlign, sCohesion, sSeparation

let ALIGN_FORCE = 3
let COHESION_FORCE = 2.7
let SEPARATION_FORCE = 3
function setup() {
	createCanvas(windowWidth, windowHeight - 50)
	for (let i = 0; i < 250; i++) {
		boids.push(new Boid(random(4, 16)))
	}
	sAlign = createSlider(0, 5, ALIGN_FORCE, 0.1)
	sCohesion = createSlider(0, 5, COHESION_FORCE, 0.1)
	sSeparation = createSlider(0, 5, SEPARATION_FORCE, 0.1)
}

function draw() {
	ALIGN_FORCE = sAlign.value()
	COHESION_FORCE = sCohesion.value()
	SEPARATION_FORCE = sSeparation.value()
	background(0)
	noFill()
	stroke(255)
	for (let b of boids) {
		b.flock(boids)
	}
	for (let b of boids) {
		b.update()
		b.render()
	}
}
