const MAX_SPEED = 3
const MAX_FORCE = 1.7
const SIGHT_DISTANCE = 25
class Boid {
    constructor(m, x, y) {
        this.mass = m || 5
        if (x === undefined || y === undefined) { this.pos = createVector(random(width), random(height))}
        else { this.pos = createVector(x, y) }
        this.vel = createVector(random(-MAX_SPEED, MAX_SPEED), random(-MAX_SPEED, MAX_SPEED))
        this.acc = createVector()
    }

    update() {
        this.vel.add(this.acc)
        this.acc.mult(0)

        this.vel.limit(MAX_SPEED)
        this.pos.add(this.vel)
        this.edges()
    }

    applyForce(f) {
        const force = p5.Vector.div(f, this.mass)
        this.acc.add(force)
    }

    render() {
        // ellipse(this.pos.x, this.pos.y, this.mass)
        push()
        translate(this.pos.x, this.pos.y)
        rotate(this.vel.heading() + TWO_PI / 4)
        beginShape()
        let w = this.mass
        let h = this.mass * 2
        vertex(w / 2, 0)
        vertex(0, h)
        vertex(w, h)
        endShape(CLOSE)
        pop()
    }

    edges() {
        if (this.pos.x > width) { this.pos.x = 0 }
        else if (this.pos.x < 0) { this.pos.x = width }
        if (this.pos.y > height) { this.pos.y = 0 }
        else if (this.pos.y < 0) { this.pos.y = height }
    }

    // Flocking
    flock(boids) {
        let align = createVector()
        let cohesion = createVector()
        let separation = createVector()
        let total = 0
        const sight = SIGHT_DISTANCE * SIGHT_DISTANCE
        for (const other of boids) {
            if (other != this) {
                const a = other.pos.x - this.pos.x
                const b = other.pos.y - this.pos.y
                let d = a * a + b * b
                if (d < sight) {
                    total++
                    align.add(other.vel)
                    cohesion.add(other.pos)
                    let diff = p5.Vector.sub(this.pos, other.pos);
                    diff.div(d);
                    separation.add(diff);
                }
            }
        }

        if (total > 0) {
            align.div(total)
            cohesion.div(total)
            separation.div(total)
            
            cohesion.sub(this.pos)

            align.setMag(MAX_SPEED)
            cohesion.setMag(MAX_SPEED)
            separation.setMag(MAX_SPEED)
            
            // align.sub(this.velocity)
            // cohesion.sub(this.velocity)
            // separation.sub(this.velocity)
            
            align.limit(MAX_FORCE)
            cohesion.limit(MAX_FORCE)
            separation.limit(MAX_FORCE)

            align.mult(ALIGN_FORCE)
            cohesion.mult(COHESION_FORCE)
            separation.mult(SEPARATION_FORCE)

            this.applyForce(align)
            this.applyForce(cohesion)
            this.applyForce(separation)

            
        }
    }
}