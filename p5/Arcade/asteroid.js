class Asteroid {
    constructor(pos, radius) {
        let speed = 2;
        this.pos = pos || createVector(random(width), random(height));
        this.velocity = createVector(random(-speed, speed), random(-speed, speed));
        this.radius = radius || random(20, 80);
        this.corners = random(5, 12);
        this.radiuses = [];
        for(let i = 0; i < this.corners; i++) {
            let r = random(this.radius/ 2, this.radius);
            this.radiuses.push(r);
        }

        this.angle = 0;
        this.rotation = random(-0.05, 0.05);
    }

    render() {
        push() ;
        translate(this.pos.x, this.pos.y);
        noFill();
        stroke(255);
        strokeWeight(4);
        
        beginShape();

        for(let i = 0; i < this.corners; i++) {
            let r = this.radiuses[i];
            let a = this.angle + (i * (TWO_PI / this.corners));
            let x = r * cos(a);
            let y = r * sin(a);
            vertex(x, y);
        }
        this.angle += this.rotation;
        endShape(CLOSE);


        pop();
    }

    update(){
        this.pos.add(this.velocity);
        this.edges();
    }

    edges() {
        if (this.pos.x + this.radius  < 0) { this.pos.x = width + this.radius ;}
        if (this.pos.x - this.radius > width) { this.pos.x = 0 - this.radius ;}
        if (this.pos.y + this.radius < 0) { this.pos.y = height +this.radius ;}
        if (this.pos.y - this.radius > height ) { this.pos.y = 0 - this.radius ;}
    }

    divide() {
        let as = [];
        if (this.radius > 20) {
            as.push(new Asteroid(this.pos.copy(), this.radius/2));
            as.push(new Asteroid(this.pos.copy(), this.radius/2));
        }
        return as;
    }
}