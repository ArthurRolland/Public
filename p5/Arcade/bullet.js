class Bullet {
    constructor(pos, angle) {
        this.pos = pos.copy();
        this.velocity = createVector(10, 0);
        this.velocity.rotate(angle);
    }

    render() {
        push();
        noFill();
        stroke(255);
        strokeWeight(6);
        point(this.pos.x, this.pos.y);
        pop();
    }

    update() {
        this.pos.add(this.velocity);
        
    }

    offscreen() {
        if (this.pos.x < 0 || this.pos.x > width || this.pos.y < 0 || this.pos.y > height) { return true; }
        return false;
    }
    

    collide(asteroid) {
        return dist(this.pos.x, this.pos.y, asteroid.pos.x, asteroid.pos.y) <= asteroid.radius;
    }
}