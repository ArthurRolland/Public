class Box {
	constructor(x, y, v) {
		this.x = x
		this.y = y
		this.value = v
		this.grid = null
	}

	isValid() { return this.grid.validBox(this) }

	toString() { return "(" + this.x + "," + this.y + ") -> " + this.value }

	draw(size) {
		if (this.isValid()) { fill(255) }
		else { fill(255,0,0) } 
		stroke(0)
		strokeWeight(1)

		let x = this.x * size
		let y = this.y * size

		rect(x, y, size, size)
		textSize(size * 0.5)
		fill(0)
		textAlign(CENTER, CENTER)
		if (this.value != 0) { text("" + this.value, x, y, size, size) }
	}
}

class Grid {
	constructor(boxes, name) {
		this.name = (name !== undefined) ? name : "Anon"
		this.boxes = boxes
		for (const b of this.boxes) { b.grid = this }
		this.count = 0
	}

	// - Helpers static
	static makeGrid(size) {
		let array = []

		for (let x = 0; x < size; x++) {
			for (let y = 0; y < size; y++) {
				array.push(new Box(x,y, 0))
			}
		}
		return new this(array)
	}

	static makeGrid(jsonObject) {
		let array = jsonObject.grid
		let boxes = []

		for (let x = 0; x < array.length; x++) {
			let line = array[x]
			for (let y = 0; y < line.length; y++) {
				boxes.push(new Box(y,x, line[y]))
			}
		}

		return new this(boxes, jsonObject.name)
	}

	// - Logic
	allForX(x) { return this.boxes.filter((b) => { return b.x == x }) }
	allForY(y) { return this.boxes.filter((b) => { return b.y == y }) }
	allForCluster(x, y) { return this.boxes.filter( (b) => { return (b.x >= x * 3) && (b.x < (x + 1) * 3) && (b.y >= y * 3) && (b.y < (y + 1) * 3)}) }
	
	validArray(array) {
		let values = []
		for (const b of array) {
			if (b.value == 0) { continue }
			if (values.includes(b.value)) { return false }
			values.push(b.value)
		}
		return true
	}

	lineValid(y) { return this.validArray(this.allForY(y)) }
	colValid(x) { return this.validArray(this.allForX(x)) }
	clusterValid(x, y) { return this.validArray(this.allForCluster(x, y)) }

	validBox(box) {
		if (!this.lineValid(box.y)) { return false }
		if (!this.colValid(box.x)) { return false }
		if (!this.clusterValid(parseInt(box.x / 3), parseInt(box.y / 3))) { return false }
		return true
	}

	isDone() {
		for (const b of this.boxes) {
			if (b.value == 0) { return false }
		}
		return true
	}

	solve() {
		this.recursiveSolve(0)
		this.draw(width - 1, height - 1)
		console.log("Done in " + this.count)
	}

	recursiveSolve(i) {
		this.count++
		if (i >= this.boxes.length) { return true }
		let b = this.boxes[i]
		if (b.value != 0) { return this.recursiveSolve(i + 1) }
		for (let n = 1; n <= 9; n++) {
			b.value = n
			if (b.isValid()) {
				let next = this.recursiveSolve(i + 1)
				if (next) { return true }
			}
		}
		b.value = 0
		return false
	}

	// - Drawing and stuff
	draw(width, height) {
		let dim = sqrt(this.boxes.length)
		let w = width / dim
		let h = height / dim
		let s = min(w, h)
		for (const b of this.boxes) {
			b.draw(s)
		}
	}
	toString() {
		let s = "Grid: \n"	
		for (const box of this.boxes) {s += box + "\n" }
		return s
	}
}
