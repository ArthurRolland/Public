let jsonData
let grid

function preload() {
	jsonData = loadJSON("sudoku.json")

	let b = createButton("solve")
	b.mousePressed(() => { grid.solve()})
}

function setup() {
	createCanvas(400, 400)
	background(0)
	noLoop()
	grid = Grid.makeGrid(jsonData.grids[4])
	grid.draw(width - 1, height - 1)
}