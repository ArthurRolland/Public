

class Body {
    constructor(center, mass, fixed = false) {
        this.center = center
        this.mass = mass
    }
    draw() {
        circle(this.center.x, this.center.y, this.mass / 5)
    }
}

class Arm {
    constructor(size, angle, endMass, parent = null) {
        this.r = size
        this.a = angle
        this.vel = 0
        this.body = new Body(createVector(0,0), endMass)
        this.child = null
        this.parent = parent

        this.inheritedVelocity = null
        this.lastVel = 0

        if (parent) {  
            this.origin = parent.body.center
            parent.child = this 
        }
        else { this.origin = createVector(0,0) }

        this.computeBodyCenter()
    }

    computeBodyCenter() {
        this.body.center.x = this.origin.x + this.r * cos(this.a)
        this.body.center.y = this.origin.y + this.r * sin(this.a)
    }

    draw() {
        let s = this.origin
        let e = this.body.center
        line(s.x, s.y, e.x, e.y)
        this.body.draw()
        if (this.child) { this.child.draw() }
    }
    
    update() {
        // let x = ((-1 * GRAVITY) / this.r) * sin(this.a)
        let calculatedVelocity = null
        if (this.child) {
            let m1 =this.body.mass / 10
            let m2 = this.child.body.mass / 10
            let a1 = this.a
            let a2 = this.child.a
            let v1 = this.vel
            let v2 = this.child.vel
            let r1 = this.r * 10
            let r2 = this.child.r * 10

            let n1 = -GRAVITY * (2 * m1 + m2) * sin(a1)
            let n2 = -m2 * GRAVITY * sin(a1 - 2 * a2)
            let n3 = -2 * sin(a1 - a2) * m2
            let n4 = v2 * v2 * r2 + v1 * v1 * r1 * cos(a1 - a2)
            let d1 = r1 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2))

            let x1 = (n1 + n2 + n3 * n4) / d1

            let n5 = 2 * sin(a1 - a2)
            let s1 = v1 * v1 * r1 * (m1 + m2)
            let s2 = GRAVITY * (m1 + m2) * cos(a1)
            let s3 = v2 * v2 * r2 * m2 * cos(a1 - a2)
            let n6 = (s1 + s2 + s3)
            let d2 = r2 * (2 * m1 + m2 - m2 * cos(2 * a1 - 2 * a2))

            let x2 = (n5 * n6) / d2

            calculatedVelocity = x1
            this.child.inheritedVelocity = x2
        }


        if (calculatedVelocity !== null && this.inheritedVelocity !== null) { this.vel += (calculatedVelocity + this.inheritedVelocity) / 2 }
        else {
            if (calculatedVelocity !== null) { this.vel += calculatedVelocity }
            if (this.inheritedVelocity != null) { this.vel += this.inheritedVelocity }
        }

        
        this.vel = Math.min(Math.max(this.vel, -1), 1)
        this.a += this.vel
        this.a = this.a % TWO_PI
        // this.vel *= 0.999
        this.lastVel = this.vel
        this.computeBodyCenter()

        if (this.child) { this.child.update() }
    }

    end() {
        if (this.child) { return this.child.end() }
        return this.body.center
    }
}