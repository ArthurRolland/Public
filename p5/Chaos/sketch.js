const GRAVITY = 3

let arm = null
let cg
let last = null
function setup() {
	angleMode(RADIANS)

	createCanvas(900, 900)
	cg = createGraphics(width, height)
	cg.background(125)
	cg.stroke(0)
	cg.strokeWeight(2)
	cg.translate(width / 2, 200)
	cg.rotate(HALF_PI)


	let d = TWO_PI / 360
	let presets = [
		[100, 180, 50],
		[80, 179.999, 40],
		// [80, 180.001, 40],
		// [25, 180 , 30],
		// [12.5, 180.01 , 20],
		// [50, 137 , 15],
		// [40, 128 , 50],
	]

	let last = null
	for (const p of presets) {
		let a = new Arm(p[0], p[1] * d, p[2], last)
		last = a
		if (!arm) { arm = a }
	}
	
}


function draw() {

	image(cg, 0, 0)
	translate(width / 2, 200)
	rotate(HALF_PI)
	
	
	stroke(255)
	strokeWeight(3)

	arm.draw()
	let end = arm.end()
	if (last) { cg.line(last.x, last.y, end.x, end.y) }
	else { cg.point(end.x, end.y) }
	last = { x: end.x, y: end.y }

	arm.update()

}

