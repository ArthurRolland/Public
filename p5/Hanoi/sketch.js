const NUMBER_OF_BLOCKS = 3
const FRAME_RATE = 25

const blockHeight = 30
let maxWidth, minWidth
let p1 = { name: "A", blocks: [] }
let p2 = { name: "B", blocks: [] }
let p3 = { name: "C", blocks: [] }

let p1Center, p2Center, p3Center

let queue = []
let run = false

function setup() {
	createCanvas(1200, 600);
	let w = width / 3
	maxWidth = w * 0.9
	minWidth = w * 0.1

	p2Center = width / 2
	p1Center = p2Center - w
	p3Center = p2Center + w

	let last = maxWidth
	for (let i = 1; i < NUMBER_OF_BLOCKS + 1; i++) {
		let s = map(i, 1, NUMBER_OF_BLOCKS, maxWidth, minWidth)

		if (last - s > minWidth) {
			s = last - minWidth
		}
		last = s

		let r = map(i, 1, NUMBER_OF_BLOCKS, 0, 255)
		let g = 0
		let b = map(i, 1, NUMBER_OF_BLOCKS, 255, 0)
		p1.blocks.push({ size:s, color: color(r,g,b) })
	}
	let b = createButton("Start")
	b.mousePressed(start)

	hanoi(NUMBER_OF_BLOCKS, p1, p2, p3)
	queue.reverse()
	frameRate(FRAME_RATE)
}

function draw() {
	background(57);

	fill(255)
	noStroke()
	let margin = minWidth / 2
	let top = height - blockHeight - margin
	let postHeight = blockHeight * 10
	let postWidth = blockHeight / 2

	rect(margin, top , width - minWidth, blockHeight)

	rect(p1Center - postWidth / 2, top - postHeight, postWidth, postHeight)
	rect(p2Center - postWidth / 2, top - postHeight, postWidth, postHeight)
	rect(p3Center - postWidth / 2, top - postHeight, postWidth, postHeight)

	stroke(0)
	let bottom = top
	for (const block of p1.blocks) {
		fill(block.color)
		let x = p1Center - block.size / 2
		rect(x, bottom - blockHeight, block.size, blockHeight)
		bottom -= blockHeight
	}

	bottom = top
	for (const block of p2.blocks) {
		fill(block.color)
		let x = p2Center - block.size / 2
		rect(x, bottom - blockHeight, block.size, blockHeight)
		bottom -= blockHeight
	}

	bottom = top
	for (const block of p3.blocks) {
		fill(block.color)
		let x = p3Center - block.size / 2
		rect(x, bottom - blockHeight, block.size, blockHeight)
		bottom -= blockHeight
	}
	if (run && queue.length > 0) {
		let f = queue.pop()
		f()
	}
}

function move(from, to) {
	console.log("Move from " + from.name + " to " + to.name )
	let block = from.blocks.pop()
	if (block) { to.blocks.push(block) }
	
}
function hanoi(n, from, via, to) {
	if (n == 0) { return }
	
	hanoi(n-1, from, to, via)
	let f = function() {
		move(from, to)
	}
	queue.push(f)
	hanoi(n-1, via, from, to)
}

function start() {
	run = true
}
