const TILES_COLOR = [
    [245, 128, 128],
    [245, 155, 128],
    [245, 188, 128],
    [245, 222, 128],
    [190, 245, 128],
    [128, 245, 175],
    [128, 245, 233],
    [128, 200, 245],
    [177, 128, 245],
    [239, 128, 245],
    [245, 128, 140],
];

class Token {
    constructor(tile) {
        this.value = random(1) < 0.3 ? 4 : 2;
        this.tile = null;
        tile.setToken(this);
    }
    tileColor() {
        let i = factorOf2(this.value) - 1;
        let c = TILES_COLOR[i];
        return color(c[0], c[1], c[2]);
    }
    textSize() {
        let s = "" + this.value;
        let d = s.length;
        return 64 * map(d, 1, 4, 1, 0.5);
    }
}

class Line {
    constructor() {
        this.tiles = []
    }
    addTile(tile) {
        this.tiles.push(tile);
    }
    move(dir) {
        if (dir < 0) {
            // From end to start
            for(let i = 1; i < this.tiles.length; i++) {
                let current = this.tiles[i];
                if (current.token) {
                    for (let j = i - 1; j >= 0; j--) {
                        let next = this.tiles[j];
                        if (next.token != null) {
                            if (next.token.value == current.token.value) {
                                next.merge(current.token);
                                current = next;
                            }
                            else {
                                break;
                            }
                        }
                        else {
                            next.setToken(current.token);
                            current = next;
                        }
                    }
                }
            }
        }
        else {
            // From end to start
            for(let i = this.tiles.length -1; i >= 0; i--) {
                let current = this.tiles[i];
                if (current.token) {
                    for (let j = i + 1; j < this.tiles.length; j++) {
                        let next = this.tiles[j];
                        if (next.token!= null) {
                            if (next.token.value == current.token.value) {
                                next.merge(current.token);
                                current = next;
                            }
                            else {
                                break;
                            }
                        }
                        else {
                            next.setToken(current.token);
                            current = next;
                        }
                    }
                }
            }
        }
    }


}

class Game {
    constructor(columns, rows, width, height) {
        this.size = {
            columns: columns, 
            rows: rows, 
            width: width, 
            height: height,
            tileW: width / columns,
            tileH: height / rows
        };
        this.tiles = [];
        this.rows = [];
        this.columns = [];
        this.tokens = [];
        this.createGame();
    }

    createGame() {
        for (let r = 0; r < this.size.rows; r++) { this.rows.push(new Line()); }
        for (let c = 0; c < this.size.columns; c++) { this.columns.push(new Line()); }
        for (let r = 0; r < this.size.rows; r++) {
            for (let c = 0; c < this.size.columns; c++) {
                let t = new Tile(c, r, this);
                this.rows[r].addTile(t);
                this.columns[c].addTile(t);
                this.tiles.push(t);
            }
        }
        this.addNew();
        this.addNew();
        this.addNew();

    }

    emptyTiles() { return this.tiles.filter( t => t.token == null ) }
    addNew() {
        let possibles = this.emptyTiles();
        let empty = random(possibles);
        if (empty) {
            let token = new Token(empty);
            this.tokens.push(token);
        }
    }

    move(x, y) {
        let lines = this.rows;
        let dir = x;
        if (y != 0) { 
            lines = this.columns;
            dir = y;
        }

        for (let line of lines) {
            line.move(dir);
        }
        this.addNew();
        this.draw();
    }

    remove(token) {
        token.tile.token = null;
        let i = this.tokens.indexOf(token)
        this.tokens.splice(i, 1);
    }

    draw() {
        background(255);
        background(195, 50);
        for (let t of this.tiles) { t.render(); }
    }
}

class Tile {
    constructor(col, row, game) {
        this.game = game;
        this.column = col;
        this.row = row;
        this.token = null;
    }
    setToken(token) {
        if (token.tile) { token.tile.token = null }
        this.token = token;
        token.tile = this;
    }

    merge(token) {
        this.token.value *= 2;
        this.game.remove(token)
    } 

    render() {
        let w = this.game.size.tileW;
        let h = this.game.size.tileH;

        let x = this.column * w;
        let y = this.row * h;

        noFill();
        stroke(57);
        rect(x, y, w, h);

        if (this.token) {
            stroke(0);
            fill(this.token.tileColor());
            rect(x + 5, y + 5, w - 10, h - 10, 30);
            textAlign(CENTER, CENTER);
            textSize(this.token.textSize());
            noStroke();
            fill(255);

            text(this.token.value, x + 10, y, w - 10, h);
        }
    }
    
}


function factorOf2(number) {
    let x = floor(number / 2)
    return (x == 1) ? 1 :  1 + factorOf2(x); 
}