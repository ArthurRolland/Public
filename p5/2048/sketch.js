const ROWS = 4;
const COLUMNS = 4;
const SIZE = 500;

let game;

function setup() {
	createCanvas(SIZE + 1, SIZE + 1);
	noLoop();
	game = new Game(COLUMNS, ROWS, SIZE, SIZE);
	game.draw()
}

function keyPressed() {
	switch (key) {
		case "ArrowUp":
			game.move(0, -1);
			break;
		case "ArrowRight":
			game.move(1, 0);
			break;
		case "ArrowDown":
			game.move(0, 1);
			break;
		case "ArrowLeft":
			game.move(-1, 0);
			break;
		default:
			break;
	}
}

