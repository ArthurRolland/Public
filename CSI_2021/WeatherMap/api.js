function search() {
    let address = document.getElementById("address").value
    currentResult = { search: address }
    display(JSON.stringify(currentResult,null, 4))
    getCoords(address)
}

function display(data) {
    document.getElementById("results").innerText = data
}

const OPEN_WEATHER_MAP_START = "http://api.openweathermap.org/data/2.5/weather?"
const OPEN_WEATHER_MAP_END = "&units=metric&appid=def617afe4374f6df3ad63c936680c85"

const GOOGLE_GEO_START = "https://maps.googleapis.com/maps/api/geocode/json?address="
const GOOGLE_API_END = "&key=AIzaSyCH-6zOfD-GexqX4pWl0aivsrcuwsZiUQI"

let currentResult = null

function getCoords(address) {
    let url = GOOGLE_GEO_START + encodeURIComponent(address) + GOOGLE_API_END
    fetch(url)
    .then(response => response.json())
    .then(data => {
        currentResult.address = data.results[0].formatted_address
        currentResult.coords = data.results[0].geometry.location
        display(JSON.stringify(currentResult,null, 4))
        getWeather(currentResult.coords.lat, currentResult.coords.lng)
    })
}

function getWeather(lat, lon) {
    let url = OPEN_WEATHER_MAP_START + "lat=" + lat + "&lon=" + lon + OPEN_WEATHER_MAP_END
    
    fetch(url)
    .then(response => response.json())
    .then(data => {
        currentResult.weather = data.weather[0].main
        currentResult.temp = data.main.temp
        display(JSON.stringify(currentResult,null, 4))
    })
}