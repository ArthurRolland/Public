<?php
    class DatabaseManager {

        // ! Properties
        private $pdo;

        // ! Init
        public function __construct(){ }

        // ! Pdo Management
        // Ne pas modifier ces methodes
        private function getPdo() {
            if ($this->pdo == NULL) {$this->pdo = $this->connectDB();}
            return $this->pdo;
        }
        private function connectDB() {
            $dsn = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
            try {
                $db = new PDO($dsn, DB_USER, DB_PASS);
                return $db;
            } catch (PDOException $e) {
                echo "Error connecting database: " . $e->getMessage();
            }
            return false;
            
        }
        
        // ! Requests
        public function getAllDrawings() {
            $request = $this->getPdo()->prepare('SELECT id, author FROM drawings');
            $request->execute();
            $resp = $request->fetchAll(PDO::FETCH_ASSOC);
            return $resp;
        }
        
        public function getDrawingForId($id) {
            $request = $this->getPdo()->prepare('SELECT * FROM drawings WHERE id = :id');
            $request->execute(['id' => $id]);
            $resp = $request->fetchAll(PDO::FETCH_ASSOC);
            if (count($resp) == 0) { return NULL; }
            $obj = $resp[0];
            $obj['data'] = json_decode(base64_decode($obj['data']));
            return $obj;
        }

        public function addDrawing($author, $json) {
            $request = $this->getPdo()->prepare('INSERT INTO drawings (author, data) VALUES (:auth, :data)');
            $request->execute(['auth' => $author, 'data' => base64_encode(json_encode($json))]);
            return $this->getPdo()->lastInsertId(); 
        }
        
    }
?>