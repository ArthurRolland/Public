#   -------------------------------
#   ADD ALL CONFIGS
#   -------------------------------

#   Load Env 
    if [ -f $HOME/.bash/.env ]; then
        source $HOME/.bash/.env;
    fi

#   Load Paths
    if [ -f $HOME/.bash/.path ]; then
        source $HOME/.bash/.path;
    fi

#   Load Aliases
    if [ -f $HOME/.bash/.alias ]; then
        source $HOME/.bash/.alias;
    fi

