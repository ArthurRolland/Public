import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';

export default class Cell extends Component {
	constructor(props) {
		super(props)
		this.data = this.props.data
	}

	selectCell() {
		if (this.props.onSelect) {
			this.props.onSelect(this.data.index)
		}
	}

	render() {
		const color1 = this.data.index % 2 == 0 ? 'white' : 'black'
		const color2 = this.data.index % 2 == 0 ? 'black' : 'white'
		return (
			<TouchableOpacity style={{flexDirection: 'column', flex: 1, height: 50, backgroundColor: color1}} onPress={this.selectCell.bind(this)}>
				<Text style={{flex: 1, textAlign: 'center', color: color2, fontSize: 20}}>{this.data.item.title}</Text>
                <Text style={{flex: 1, textAlign: 'center', color: color2, fontSize: 15}}>{this.data.item.subtitle}</Text>
			</TouchableOpacity>
		);
	}
}
