import React, {Component} from 'react';
import {StyleSheet, View, Text, FlatList, TextInput, Button, ActivityIndicator} from 'react-native';
import Cell from '../components/Cell.js';
import MySegmentControl from '../components/MySegmentControl';
import Sound from 'react-native-sound';

// http://ctexdev.net/arthur/Kaamelott/sound/sounds.json
export default class List extends Component {
	constructor(props) {
		super(props)
		this.state = {searchText: "", isLoaded: false, isLoading: false, data:[], key: 0};
		this.dataSource = [];
		
		this.soundQueue = [];
		this.isPlaying = false;
		this.order = 0;
		this.isLooping = false;

		this.playSoundAtIndex = this.playSoundAtIndex.bind(this)
		this.orderAndSearch = this.orderAndSearch.bind(this)
		this.playNext = this.playNext.bind(this)
		this.finishedPlaying = this.finishedPlaying.bind(this)
	}

	// ! Playing sound
	loop() {
		this.isLooping = !this.isLooping
		if(this.isLooping) {
			const x = Math.floor((Math.random() * 10000) % this.dataSource.length)
			this.soundQueue.push(this.dataSource[x])
			this.playNext()
		}
	}
	selectCellAtIndex(index) {
		this.playSoundAtIndex(index)
	}
	playSoundAtIndex(index) {
		const item  = this.state.data[index]
		this.soundQueue.push(item)
		this.playNext()
	}
	playNext() {
		if (!this.isPlaying && this.soundQueue.length > 0) {
			const item = this.soundQueue.shift()
			const file = 'http://ctexdev.net/arthur/Kaamelott/sound/' + item.file;
			const sound = new Sound(file, null, (error) => {
				if (error) { console.error(error); }
				this.isPlaying = true
				sound.play(this.finishedPlaying)
			});
		}
	}
	finishedPlaying() {
		this.isPlaying = false
		if(this.isLooping) {
			const x = Math.floor((Math.random() * 10000) % this.dataSource.length)
			this.soundQueue.push(this.dataSource[x])
		}
		this.playNext()
	}

	// ! Network
	loadData() {
		this.setState({
			...this.state,
			isLoading: true
		});
		fetch('http://ctexdev.net/arthur/Kaamelott/sound/sounds.json')
      	.then((response) => response.json())
      	.then((responseJson) => {
			console.log(responseJson)
			this.setData(responseJson)
		})
      	.catch((error) =>{
        	console.error(error);
      	});
	}
	setData(data) {
		this.dataSource = data
		this.orderAndSearch()
	}

	// ! Search and order
	changeText(text) {
		this.setState({ 
			...this.state,
			searchText: text
		});
	}
	selectionChange(index) {
		this.order = index
		this.orderAndSearch()
	}
	search() {
		this.orderAndSearch()
	}

	orderAndSearch(){
		// if this.order == 0
		let filter = (item, idx) => item.title.toLowerCase().includes(this.state.searchText.toLowerCase());
		let sort = (item1, item2) => item1.title.localeCompare(item2.title)
		
		if (this.order == 1) {
			filter = (item, idx) => item.character.toLowerCase().includes(this.state.searchText.toLowerCase());
			sort = (item1, item2) => item1.character.localeCompare(item2.character)
		}
		else if (this.order == 2) {
			filter = (item, idx) => item.episode.toLowerCase().includes(this.state.searchText.toLowerCase());
			sort = (item1, item2) => item1.episode.localeCompare(item2.episode)
		
		}

		let data = this.dataSource.filter(filter)
		data = data.sort(sort)
		

		this.setState({
			... this.state,
			isLoaded: true,
			isLoading: false,
			data: data,
			key: Math.random()
		});
	}

	
	renderCell(data) {
		const cellData = {
			index: data.index, 
			item: {
				title: data.item.title, 
				subtitle: data.item.character + " (" + data.item.episode + ")"
			}
		};
		return <Cell data={cellData} onSelect={this.selectCellAtIndex.bind(this)}/>
	}

					
	render() {
		content = (
			<View style={styles.mid}>
				<Button title="Load data" onPress={this.loadData.bind(this)} />
			</View>
		);

		if (this.state.isLoaded) {
			let button = this.state.searchText == "" ? [] : <Button style={{marginRight: 10}} title="Search" onPress={this.search.bind(this)}/>
			content = (
				<View style={styles.list}>
					<View style={styles.search}>
						<TextInput style={styles.field} placeholder="Search" onChangeText={this.changeText.bind(this)}></TextInput>
						{button}
					</View>
					<View style={styles.segment}>
						<MySegmentControl style={{flex:1, margin: 5}} values={["Titre", "Auteur", "Episode"]} onSelectionChange={this.selectionChange.bind(this)} activeColor="black"/>
					</View>
					<FlatList 
						style={styles.content} 
						data={this.state.data} 
						renderItem={this.renderCell.bind(this)}
						keyExtractor={(item, index) => item.title} 
						key={this.state.key}
					/>
					<View style={{...styles.search, flexDirection: 'column', justifyContent: 'center'}}>
						<Button title="Loop" onPress={this.loop.bind(this)} />
					</View>
				</View>
			);
		}
		else if (this.state.isLoading) {
			content = (
				<View style={styles.mid}>
					<ActivityIndicator />
				</View>
			); 
		}
		return (
			<View style={styles.container}>
				<View style={styles.top}>
					<Text style={styles.title}>List</Text>
				</View>
				{content}
			</View>
			);
		}
	}
	
	const styles = StyleSheet.create({
		container: {
			flex: 1,
			flexDirection: 'column',
			backgroundColor: '#F5FCFF',
		},
		top: {
			flex: 1,
			justifyContent: 'flex-end',
			borderBottomColor: 'darkgray',
			borderBottomWidth: 1,
		},
		mid: {
			flex: 7,
			flexDirection: 'column',
			alignItems: 'center',
			justifyContent: 'center',
		},
		list: {
			flex: 7,
			flexDirection: 'column',
			alignItems: 'stretch',
		},
		search: {
            height: 50,
            backgroundColor: 'lightgray',
			flexDirection: 'row',
			alignItems: 'center',
		},
		segment: {
			height: 40,
            backgroundColor: 'lightgray',
		},
        field: {
			flex: 1,
			height: 40,
            backgroundColor: 'white', 
            marginHorizontal: 5,
			paddingVertical: 5, 
            paddingHorizontal: 5,
            borderColor: 'darkgray', 
            borderWidth: 1, 
            borderRadius: 5, 
        },
        content: {
            flex: 1,
        },
		title: {
			fontSize: 25,
			fontWeight: 'bold',
			textAlign: 'center',
			marginBottom: 10,
		},
		item: {
			padding: 10,
			fontSize: 18,
			height: 44,
		},
	});
	