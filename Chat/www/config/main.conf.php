<?php

    // ! Error reporting
    // We activate different settings to be sure that the errors are shown
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    // ! FileSystem Const
    // We create constants referring to differents folders of our file structure
    define('INCLUDE_PATH', realpath(dirname(dirname(__FILE__))) . '/');
    define('CLASSES_PATH', INCLUDE_PATH . 'classes/');
    define('CONFIG_PATH', INCLUDE_PATH . 'config/');
    define('SCRIPTS_PATH', INCLUDE_PATH . 'scripts/');


    // ! MySQL Const
    const DB_DRIVER = 'mysql';
    const DB_HOST = 'localhost';
    const DB_NAME = 'Chat';
    const DB_USER = 'root';
    const DB_PASS = 'root';

    // ! Autoload
    // We register and implement the classes autoloading function, that will be in charge of registering unknown classes at runtime
    spl_autoload_register('loadClasses');
    function loadClasses($className) {  require_once(CLASSES_PATH . $className.'.class.php');  }

?>